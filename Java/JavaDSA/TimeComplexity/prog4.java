// Optimised Approach
// write the difference between two number  [3,5]=3 [1,10]=10 [5,10=6]

import java.util.*;
class prog4{
	
		static int diff(int n1, int n2){
			int dif=0;
			if(n1<n2){
				dif=n2-n1;
			}else{
				dif=n1-n2;
			}
			return dif+1;
		}

	public static void main(String [] args){

		Scanner sc = new Scanner(System.in);
	
		System.out.println("Enter a num1");
		int num1=sc.nextInt();
		System.out.println("Enter a num2");
		int num2=sc.nextInt();

		int r=diff(num1,num2);
		System.out.println("diff "+r);
	}
}
