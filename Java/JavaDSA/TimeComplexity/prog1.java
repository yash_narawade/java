//Brute Force Approach
// write code to caluclate the sum of n natural number

import java.io.*;
class prog1{

	static int sum(int num){
		int add=0;
		for(int i=1; i<=num; i++){
			add+=i;
		}
		return add;
	}
	public static void main(String [] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a natural number ");
		int num=Integer.parseInt(br.readLine());
		int r=sum(num);
		System.out.println("sum  "+r);

	}
}
