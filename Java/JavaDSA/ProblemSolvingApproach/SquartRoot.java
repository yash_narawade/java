// SquareRoot of  Number 
// Brute Force
class Demo{

	static int square(int num){
		int itr=0;
		int ret=0;
		for(int i=1; i*i<=num; i++){
			itr++;
			ret=i;
		}	
			
		System.out.println(itr);
		return ret;
	
	}

	public static void main(String [] args){
	
		int num = 100;
		System.out.println(square(num));
	}
}
