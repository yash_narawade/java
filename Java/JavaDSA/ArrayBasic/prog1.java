/*
 1] Search an Element in an array

Given an integer array and another integer element. The task is to find if the given
element is present in the array or not.

Example 1:
Input:
n = 4
arr[] = {1,2,3,4}
x = 3
Output: 2

Explanation: There is one test case with an array as {1, 2, 3 4} and an
element to be searched as 3. Since 3 is present at index 2, output is 2.

Example 2:
Input:
n = 5
arr[] = {1,2,3,4,5}
x = 5
Output: 4

Explanation: For array elements {1,2,3,4,5} element to be searched is 5 and
it is at index 4. So, the output is 4.
Expected Time Complexity: O(n).
Expected Auxiliary Space: O(1).

Constraints:
1 <= n <= 10^6
0 <= arr[i] <= 10^6
0 <= x <= 10^5
 */

import java.util.*;
class prog1{
		static void searchEle(int arr[],int key){
		/*	Brute force
		 	
			
			int flag=0;
			int ret=0;
			int itr=0;
			for(int i=0; i<arr.length; i++){
				itr++;

				if(arr[i]==key){
					flag=1;
					ret=i;
					break;
				}
			}
			
				if(flag==1){
				
					System.out.println("Key found "+ret);
					
				} else{
				
					System.out.println("Key not found");
				}
				System.out.println("itr "+itr);*/


			//Optimised 
			int start=1;
			int end=arr.length-1;
			int mid=0;
			int ans=0;
			
			for(int i=0; start<=end; i++){
			
				itr++;
				mid=(start+end)/2;

				if(mid*mid==key){
					
					System.out.println("mid"+mid);
					break;	
				}

				if(mid*mid>=key){
				
					end=mid-1;
				}

				if(mid*mid<=key){
				
					start=mid+1;
					ans=start;
				}
			}

			System.out.println("itr "+itr);
			System.out.println("mid "+mid);
				
		}

}
class Client{

	public static void main(String [] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a array size");
		int size=sc.nextInt();

		System.out.println("Enter a array element");
		int arr[] = new int[size];

		for(int i=0; i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		System.out.println("Enter a key");
		int key=sc.nextInt();

		prog1.searchEle(arr,key);
	}
}
