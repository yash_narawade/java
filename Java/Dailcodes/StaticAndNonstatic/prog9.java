class InputDemo{

	void fun(){
	
		System.out.println("In fun function");
	}

	void gun(){
	
		System.out.println("In gun function");
	}

	void run (){
	
		System.out.println("In run function");
	}

	public static void main(String args[]){
	
		System.out.println("In main function");

		InputDemo obj= new InputDemo();

		obj.fun();
		obj.gun();
		obj.run();
	}
}
