import java.util.Scanner;
class ScannerDemo{

	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in); 

		System.out.print("Enter your name");
		String name=sc.next();
		
		System.out.print("Enter your College name");
		String clgname=sc.next();

		System.out.print("Enter stud Id");
		int studid =sc.nextInt();


		System.out.print("Enter your cgpa");
	       	float marks = sc.nextFloat();

		System.out.println("Student name "+ name);
		System.out.println("College name "+clgname);
		System.out.println("Stud id "+studid);
		System.out.println("Cgpa "+marks);
	}
}
