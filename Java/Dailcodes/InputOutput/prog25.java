import java.io.*;
class InputDemo{

	public static void main(String [] args)throws IOException{
	
		InputStreamReader isr = new InputStreamReader(System.in);

		BufferedReader br = new BufferedReader(isr);
		
		System.out.print("Enter Company name : ");
		String name=br.readLine();
		
		System.out.print("Enter Employee name : ");
		String empname=br.readLine();

		System.out.print("Enter Employee id : ");
		int id=Integer.parseInt(br.readLine());


		System.out.println("Company Name : "+name);
		System.out.println("Employee Name : "+empname);
		System.out.println("Employee ID : "+id);
		

	}
}
