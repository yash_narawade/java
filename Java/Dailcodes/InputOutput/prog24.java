import java.io.*;
class InputDemo{

	public static void main(String [] args)throws IOException{
	
		InputStreamReader isr = new InputStreamReader(System.in);

		BufferedReader br = new BufferedReader(isr);
		
		System.out.println("Enter Company name");
		String name=br.readLine();
		
		System.out.println("Enter Employee name");
		String empname=br.readLine();

		System.out.println("Enter Employee id");
		int id=br.readLine();

		System.out.println("Employee Name : "+name);
		System.out.println("Company Name : "+empname);
		

	}
}
