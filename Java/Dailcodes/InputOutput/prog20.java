import java.io.*;
class InputDemo{

	public static void main(String [] args)throws IOException{
	
		InputStreamReader isr = new InputStreamReader(System.in);

		BufferedReader br = new BufferedReader(isr);
		
		System.out.println("Enter name");
		String name=br.readLine();
		System.out.println(name);

		//br.close();
		
		System.out.println("Enter Company name");
		String comname=br.readLine();
		System.out.println(comname);
		

	}
}
