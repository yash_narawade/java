
//Write a program to take a number as input from the user and check whether the number is divisible by 8 or not. 


import java.util.*;
class prog3{

	public static void main(String [] args){
	
		Scanner sc =new Scanner(System.in);
		
		System.out.print("Enter a Number");
		int num=sc.nextInt();

		if(num%8==0){
		
		
			System.out.println(num+" is divisible by 8");
		}
		else{
		
			System.out.println(num+" Not divisible by 8");
		}

	}
}
