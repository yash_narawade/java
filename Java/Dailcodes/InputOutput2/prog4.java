
//Write a program to take a number as input from the user and print its table.


import java.util.*;
class prog4{

	public static void main(String [] args){
	
		Scanner sc =new Scanner(System.in);
		
		System.out.print("Enter a Number");
		int num=sc.nextInt();

		for(int i=1; i<=10; i++){
		
			System.out.print(num*i+",");
		}

	}
}
