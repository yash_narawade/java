class switchDemo{

	public static void main(String [] args){
	
		char data=68;
		System.out.println("Before Switch");

		switch(data){
		
			case 'A':
				System.out.println('A');
				break;
			case 'B':
				System.out.println("66");
				break;

			case 'C':
				System.out.println('C');
				break;

			case 68:
				System.out.println("68");
				break;

			default:
				System.out.println("Invalid input");

		}
	}
}
