/*
  A B C D
  a b c d
  A B C D
  a b c d
*/
class NestedFor{

	public static void main(String []a){

		for(int i=1;i<=2;i++){
			
			char capi=65;
			
			for(int j=1; j<=4; j++){
				
				System.out.print(capi++ +" ");
				
			}
			
			System.out.println();
			
			char smal=97;
			
			for(int k=1; k<=4; k++){
			
				System.out.print(smal++ +" ");
			
			}
			System.out.println();
		
		}
	
	}

}
