class StringDemo{

	public static void main(String [] args){
	
		String str1 = "Yash";              //SCP
		String str2 = "Ashish";            //SCP
		String str3 = new String("Yash");  //Heap
		String str4 = "YashAshish";        //SCP

		System.out.println(System.identityHashCode(str1)); //1000
		System.out.println(System.identityHashCode(str2)); //200
		System.out.println(System.identityHashCode(str3)); //300
		System.out.println(System.identityHashCode(str4)); //400
	}
}
