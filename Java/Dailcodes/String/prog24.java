class StringDemo{

	public static void main(String [] args){
	
		String str1 = "Rohit";                   //SCP
		String str2 = str1;			//SCP
		String str3 = new String("Virat");	//Heap
		String str4 = str3;			//Heap
		String str5 = new String(str1);		//Heap


		System.out.println(System.identityHashCode(str1));   //1000
		System.out.println(System.identityHashCode(str2));   //1000
		System.out.println(System.identityHashCode(str3));   //3000
		System.out.println(System.identityHashCode(str4));   //3000
		System.out.println(System.identityHashCode(str1));   //4000
	}
}
