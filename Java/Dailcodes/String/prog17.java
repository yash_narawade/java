class StringDemo{

	public static void main(String [] args){
		
		String str1 = "Yash";      //SCP
		String str2 = "Narawade";  //SCP
		String str3 = "Yash";      //SCP

		System.out.println(System.identityHashCode(str1)); //1000
		System.out.println(System.identityHashCode(str2)); //2000
		System.out.println(System.identityHashCode(str3)); //1000
	}
}
