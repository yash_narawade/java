class StringDemo{

	public static void main(String [] args){
	
		String str1= "Kanha";         //SCP
		String str2= "Ashish";        //SCP
		String str3= "KanhaAshish";   //SCP
		String str4=str1+str2;        //Heap  operation performed string goes on heap

		System.out.println(System.identityHashCode(str1));   //1000
		System.out.println(System.identityHashCode(str2));   //2000
		System.out.println(System.identityHashCode(str3));   //3000
		System.out.println(System.identityHashCode(str4));   //4000
	}
}
