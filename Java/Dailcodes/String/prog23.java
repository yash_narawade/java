class StringDemo{

	public static void main(String [] args){
	
		String str1 = "Badhe";      			//SCP
		String str2 = "Rahul";				//SCP
		String str3 = new String("BadheRahul");		//Heap
		String str4 = str3;				//SCP


		System.out.println(System.identityHashCode(str1));  //1000
		System.out.println(System.identityHashCode(str2));  //2000
		System.out.println(System.identityHashCode(str3));  //4000
		System.out.println(System.identityHashCode(str4));  //4000
	}
}
