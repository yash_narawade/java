class StringDemo{

	public static void main(String [] args){
	
		String str1 = new String("Yash");      //Heap
		String str2 = new String("Narawade");  //Heap
		String str3 = new String("Yash");      //Heap

		System.out.println(System.identityHashCode(str1)); //1000
		System.out.println(System.identityHashCode(str2)); //2000
		System.out.println(System.identityHashCode(str3)); //3000
	}
}
