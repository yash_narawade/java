class StringDemo{

	public static void main(String [] sra){
	
		String str = new String("Shashi");   //object1 
		System.out.println(str);   // shashi
		System.out.println(System.identityHashCode(str));  //1000
		
		str=str+"Bagal";   //object2
		System.out.println(str); //ShashiBagal
		System.out.println(System.identityHashCode(str));  //2000
		
		str=str+"c2w"; //object3
		System.out.println(str); //ShashiBagalc2w
		System.out.println(System.identityHashCode(str));  //3000

	}
}
