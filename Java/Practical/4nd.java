class c2w_ShortDemo{
	public static void main(String [] args){
	
		short sh1='A';
		short sh2='1';
		System.out.println(sh1);
		System.out.println(sh2);
	}
}


/*
o/p

65
49

Explanation 

	Here we declare 2 variable of short datatypes's and assign value " A" "1" respectively. 
	In print statement it print the ASCII value of it.
	*/
