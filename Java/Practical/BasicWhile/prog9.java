/*
 Write a program to print the sum of odd numbers from 150 to 101.
Output : 3125
*/


class BasicWhile{

	public static void main(String [] args){
		
		int i=150;
		int sum=0;	
		while(i>=101){
		
			if(i%2!=0){
			
				sum=sum+i;
			}
			i--;
			
		}	
		 System.out.println(sum);
	}
}
