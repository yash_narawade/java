class c2w_charDemo{
	public static void main(String [] args){
		char ch=65;
		char ch1=ch;
		System.out.println(ch);
		System.out.println(ch1);

	}
}
/*
o/p
A
A

Explanation 
	In this code "ch" has assigned value of "65" and then we assign "ch" value to "ch1" and print the both value it print the ASCII value of 65 i.e "A".
	*/
