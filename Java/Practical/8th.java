class c2w_Demo{
	public static void main(String [] args){
		char ch1=true;
		char ch2=false;
		System.out.println(ch1);
		System.out.println(ch2);
	}
}


/*
 *
 o/p

 8th.java:3: error: incompatible types: boolean cannot be converted to char
		char ch1=true;
		         ^
8th.java:4: error: incompatible types: boolean cannot be converted to char
		char ch2=false;
		         ^
2 errors


Explanation 
	we are trying to store to boolean value to charcter ch1 but jvm can't convert bit to byte.
	*/
