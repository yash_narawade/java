class C2W_ShortDemo{
	public static void main(String [] args){
		short sh1=19;
		short sh2=11;
		int  sh3=sh1+sh2;
		System.out.println(sh3);
	}	
}


/*
o/p
30

Explanation 
	In this code we declare 2 varialble of short datatypes and also declaer third varialble for
	storing the additon of both varialbe Here we declare integer type varialbel for addition because the after addtion the value can't strore in short
	*/
