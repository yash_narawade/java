class C2W_CharDemo{
	public static void main(String [] args){
		char ch;
		ch=65;
		System.out.println("char="+ch);
		System.out.println("char="+ch+1);
	}
}


o/p:
char=A
char=A1

Explanation :
 In main method we declar a valriable named as ch of charcter type  and 
 assign a value "65"  to the ch variable then print the ch's ASCII i.e "65" value .In next printing statement we print the value of ch ASCII and concatinates the 1. 
