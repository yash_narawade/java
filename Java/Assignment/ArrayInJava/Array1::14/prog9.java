/*
9. Write a program where you have to print the odd indexed elements. Take input from
the user
Example :
Enter size: 10.
1 2 3 4 5 6 7 8 9 10
2 is an odd indexed element
4 is an odd indexed element
6 is an odd indexed element
8 is an odd indexed element
10 is an odd indexed element
*/


import java.io.*;
class prog9{

	public static void main(String [] args )throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter a size : ");
		int size=Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];
		
		for(int i=0; i<size; i++){
		
			System.out.print("Enter array Element :");
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		for(int i=0; i<arr.length; i++){
				if(i%2==1){		
					System.out.println(arr[i]+" is an odd index element ");
				}
			
		}
				
	}
}
