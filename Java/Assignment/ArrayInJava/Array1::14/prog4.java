/*
4. Write a program to print the sum of odd elements in an array.Take input from the user.
Example:
Enter size: 10
Array:
1 2 3 4 2 5 6 2 8 10
Output :
Sum of odd elements : 9
 */


import java.io.*;
class prog4{

	public static void main(String [] args )throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter a size : ");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		
		for(int i=0; i<size; i++){
		
			System.out.print("Enter array Element :");
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		int sum=0;
		for(int i=0; i<arr.length; i++){
			if(arr[i]%2==1){
				sum=sum+arr[i];
			}
		}
				System.out.println("Sum of odd element : "+sum );
	}
}
