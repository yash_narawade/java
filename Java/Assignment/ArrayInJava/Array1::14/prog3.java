/*
3. Write a program to print the even elements in the array. Take input from the user.
Example :
Enter size : 10
Array:
10 11 12 13 14 15 16 17 18 19
Output :
10
12
14
16
18
 */


import java.io.*;
class prog3{

	public static void main(String [] args )throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter a size : ");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		
		for(int i=0; i<size; i++){
		
			System.out.print("Enter array Element :");
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		for(int i=0; i<arr.length; i++){
			if(arr[i]%2==0){
				System.out.println(arr[i]);
			}
		}
	}
}
