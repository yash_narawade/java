/*
2. Take an input from the user where the size of the array should be 10 and print the
output of the user given elements of an array.
*/


import java.io.*;
class prog2{

	public static void main(String [] args )throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int arr[]=new int[10];
		
		for(int i=0; i<arr.length; i++){
		
			System.out.print("Enter array Element :");
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i]+",");
		}
	}
}
