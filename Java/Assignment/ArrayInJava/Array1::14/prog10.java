/*
10. Write a real-time example where you have to use the array. Take input from the user.
 */


import java.io.*;
class prog10{

	public static void main(String [] args )throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter Number of programming Languages you Learn : ");
		int size=Integer.parseInt(br.readLine());
		String arr[]=new String[size];
		
		for(int i=0; i<size; i++){
		
			System.out.print("Enter Lanuguages :");
			arr[i]=br.readLine();
		}
		
		System.out.println("You have you learn "+arr.length+" languages, Which are : ");
		for(int i=0; i<arr.length; i++){
			System.out.println(arr[i] );
		}
	}
}
