/*
 9. WAP to print the minimum element in the array, where you have to take the size and
elements from the user.
Example:
Input:
Enter the size
5
Enter elements:
5
6
9
-9
17
Output:

Minimum number in the array is : -9
 */


import java.io.*;
class prog9{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a array size");
		int size = Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];
		System.out.print("Enter a array Element");

		for(int i=0; i<arr.length; i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		int min=arr[0];
		for(int i=0; i<size; i++){
	
			if(arr[i]<min){
				min=arr[i];
			}

			
		}

		System.out.println("min element "+min);
	}
}
