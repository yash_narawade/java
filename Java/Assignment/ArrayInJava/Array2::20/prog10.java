/*
 10. WAP to print the Maximum element in the array.
Example:
Input:
Enter the size
5
Enter elements:
7
81
65
12
23
Output:
Maximum number in the array is found at pos 1 is 81
 */


import java.io.*;
class prog10{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a array size");
		int size = Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];
		System.out.print("Enter a array Element");

		for(int i=0; i<arr.length; i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		int max=arr[0];
		for(int i=0; i<size; i++){
	
			if(arr[i]>max){
				max=arr[i];
			}

			
		}

		System.out.println("min element "+max);
	}
}
