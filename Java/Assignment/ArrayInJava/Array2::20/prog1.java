/*
1.WAP to count the even numbers in an array where you have to take the size and
elements from the user. And also print the even numbers too
Example:
Enter size =8
1 12 55 65 44 22 36 10
Output : even numbers 12 44 22 36 10
Count of even elements is : 5
*/


import java.io.*;
class prog1{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a array size");
		int size = Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];
		System.out.print("Enter a array Element");

		for(int i=0; i<arr.length; i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}
		int count=0;
		System.out.print("Even number :");
		for(int i=0; i<arr.length; i++){
			if(arr[i]%2==0){
				count++;
				System.out.print(arr[i]+" ");
			}
		}

		System.out.println("Count of even number : "+count);

	}
}
