/*
Q9.
Print the prime numbers in an array.
5 7 9 11 15 19 90
Output:
5 7 11 19
*/


import java.io.*;
class prog10{

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter array size");
		int size=Integer.parseInt(br.readLine());
		
		int arr[] = new int[size];
		System.out.println("Enter array Element : ");
		
		int prod=1;
		for(int i=0;i<size; i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		int cnt=0;
		for(int i=0; i<size; i++){
				
			for(int j=1;j<=arr[i];j++){

				if(arr[i]%j==0){
					cnt++;
				}
							
						
			}
			if(cnt==2){
				 prod=prod*arr[i];
			}
			cnt=0;
		}
				System.out.print(prod+" ");

	}
}
