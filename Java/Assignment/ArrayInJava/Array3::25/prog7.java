/*
Q7
Check the size of the array and if array size is odd and greater than or equal to 5, then
print the odd elements, else print the even numbers.
Example 1:
Size : 6
121 144 225 88 90 89
Output:
144 88 90
Example:
Size : 5
1 625 196 169 7
Output:
1 625 169 7
*/


import java.io.*;
class prog7{

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter array size");
		int size=Integer.parseInt(br.readLine());
		
		int arr[] = new int[size];
		System.out.println("Enter array Element : ");
		for(int i=0;i<size; i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0; i<size; i++){
		
			if(size%2==1 && size>=5){
			
				if(arr[i]%2==1){
				
					System.out.print(arr[i]+" ");
				}
			}
			else{
				if(arr[i]%2==0){
					System.out.print(arr[i]+" ");
				}
			}
		}
	}
}
