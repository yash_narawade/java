/*
Q6
Write a program to print all the consonants in an array.
a b c y x o p
Output:
b c y x p
*/


import java.io.*;
class prog6{

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter array size");
		int size=Integer.parseInt(br.readLine());
		
		char arr[] = new char[size];

		System.out.println("Enter array Element : ");
		for(int i=0;i<size; i++){
			
			arr[i]=(char)br.read();
			br.skip(1);
		}
		
		System.out.print("consant are : ");
		for(int i=0; i<size; i++){
				if(arr[i]=='a'|| arr[i]=='e'|| arr[i]=='i' || arr[i]=='o' || arr[i]=='u' || arr[i]=='A' || arr[i]=='E' || arr[i]=='I' || arr[i]=='O'||arr[i]=='U'||arr[i]=='U'){
				
					continue;
				}
				else{
				
					System.out.print(arr[i]+" ");
				}		
						
			}
		
		
	
	}
}
