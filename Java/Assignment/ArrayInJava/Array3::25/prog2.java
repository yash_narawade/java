/*
 Q2
Write a program to find the first occurrences of a specific number in an array. Print the
index of a first occurrence.
Example :
1 5 9 8 7 6
Input: Specific number : 5
Output: num 5 first occurred at index : 1
Input: Specific number : 11
Output: num 11 not found in array.
 */


import java.io.*;
class prog2{

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter array size");
		int size=Integer.parseInt(br.readLine());
		
		int arr[] = new int[size];
		System.out.println("Enter array Element : ");


		for(int i=0;i<size; i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.print("Enter a  number that want to find :" );
		int num=Integer.parseInt(br.readLine());
	
		boolean isfound=false;
		for(int i=0; i<size; i++){
				if(arr[i]==num){
				
					System.out.println("num"+num+" first occureance at "+i);
					isfound=true;
					break;
				}				
			}

		if(isfound==false){
		
			System.out.println("Not found");
		}
		
		
	
	}
}
