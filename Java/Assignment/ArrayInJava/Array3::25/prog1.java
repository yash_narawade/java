/*
Q1
Write a program to add 15 in all elements of the array and print it.
10 20 30 40 50 60
Output:
25 35 45 55 65 75
*/

import java.io.*;
class prog1{

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter array size");
		int size=Integer.parseInt(br.readLine());
		
		int arr[] = new int[size];
		System.out.print("Enter array Element : ");
		for(int i=0;i<size; i++){
		
			arr[i]=Integer.parseInt(br.readLine())+15;
		}

		for(int i=0; i<size; i++){
		
			System.out.print(arr[i]+" ");
		}
	
	}
}
