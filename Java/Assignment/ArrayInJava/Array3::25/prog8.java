/*
Q8.
Print the composite numbers in an array.
4 5 7 9 10

Output:
4 9 10
*/


import java.io.*;
class prog8{

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter array size");
		int size=Integer.parseInt(br.readLine());
		
		int arr[] = new int[size];
		System.out.println("Enter array Element : ");
		for(int i=0;i<size; i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		int cnt=0;
		for(int i=0; i<size; i++){
				
			for(int j=1;j<=arr[i];j++){

				if(arr[i]%j==0){
					cnt++;
				}
							
						
			}
			if(cnt>2){
				System.out.print(arr[i]+" ");
			}
			cnt=0;
		}

	}
}
