/*
Q8. WAP to print the occurrence of a user given character.
Example:
Input:
Enter the size of the array:
5
Enter the elements of the array:
A
Y
U
O
U
P
Enter the character to check:
U
*/
import java.util.*;
class prog8{

	public static void main(String a[]){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter a size");
		int size=sc.nextInt();

		char arr[] = new char[size];

		for(int i=0; i<size; i++){
		
			arr[i]=sc.next().charAt(0);
		}
		System.out.print("Enter character ");
		char ch= sc.next().charAt(0);
		int cnt=0;
		for(int i=0; i<size; i++){
			if(arr[i]==ch){
				cnt++;
			}	
					
		}
		System.out.println(ch+" occurs "+cnt+" time in arrray");
	

	}
}
