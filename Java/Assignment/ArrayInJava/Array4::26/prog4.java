/*
Q4. WAP to check whether the user given number occurs more than 2 times or equals 2
times.
Example :

Input:
Enter the size of the array:
6
Enter the elements of the array:
56
65
78
56
90
56
Enter the number to check:
56
Output:
56 occurs more than 2 times in the array.
*/
import java.util.*;
class prog4{

	public static void main(String a[]){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter a size");
		int size=sc.nextInt();

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
		
			arr[i]=sc.nextInt();
		}
		System.out.print("Enter a number to check :");
		int num= sc.nextInt();
		int cnt=0;
		for(int i=0; i<size; i++){
			
			if(num==arr[i]){
				cnt++;
			}			
		}

		System.out.println(num+" occurs more than "+cnt+" times in array ");

	}
}
