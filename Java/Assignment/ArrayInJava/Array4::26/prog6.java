/*
Q6. WAP to count the vowels and consonants in the given array(Take input from the user)
Example:
Enter the size of the array:
6
Enter the elements of the array:
a
E
P
o
U
G
Output:
Count of vowels: 4

Count of consonants: 2
*/
import java.util.*;
class prog6{

	public static void main(String a[]){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter a size");
		int size=sc.nextInt();

		char arr[] = new char[size];

		for(int i=0; i<size; i++){
		
			arr[i]=sc.next().charAt(0);
		}
		int Vcnt=0;
		int Ccnt=0;
		for(int i=0; i<size; i++){
			if(arr[i]=='a'||arr[i]=='e'||arr[i]=='i'||arr[i]=='o'||arr[i]=='u'||arr[i]=='A'||arr[i]=='E'||arr[i]=='I'||arr[i]=='O'||arr[i]=='U'){
				Vcnt++;
			}	
			else{
				Ccnt++;
			}		
		}
		System.out.println("Conunt of Vowel "+Vcnt);
		System.out.println("Count of Cosonant "+Ccnt);

	}
}
