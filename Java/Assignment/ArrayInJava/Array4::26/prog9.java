/*
Q9. WAP to replace the elements with #, which are not in the range of ‘a to z’.
Input:
Enter the size:
6
Enter Elements
A
B
c
d
E
*
Output
Array:
#

#
c
d
#
#
*/
import java.util.*;
class prog9{

	public static void main(String a[]){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter a size");
		int size=sc.nextInt();

		char arr[] = new char[size];

		for(int i=0; i<size; i++){
		
			arr[i]=sc.next().charAt(0);
		}
		for(int i=0; i<size; i++){
			if(arr[i]<97 || arr[i]>122){
				System.out.println("#");
			}
			else{			
			  System.out.println(arr[i]);
			}
		
		}
	

	}
}
