/*
Q5. WAP to reverse the array(take input from the user).
Example:
Input:
Enter the size of the array:
6
Enter the elements of the array:
1
2
3
4
5
6
Output:
Reversed array:
6
5
4
3
2
1
*/
import java.util.*;
class prog5{

	public static void main(String a[]){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter a size");
		int size=sc.nextInt();

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
		
			arr[i]=sc.nextInt();
		}
		int temp=0;
		for(int i=0; i<size/2; i++){
			
			temp=arr[i];
			arr[i]=arr[size-i-1];
			arr[size-i-1]=temp;			
		}
		for(int i=0; i<size; i++){
			System.out.println(arr[i]);
		}

	}
}
