/*
Q7. WAP to convert lowercase characters to UPPERCASE characters.(Take input from
the user)
Example:
Input:
Enter the size of the array:
6
Enter the elements of the array:
a
B
Y
p
o
H
Output:
A B Y P O H
*/
import java.util.*;
class prog7{

	public static void main(String a[]){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter a size");
		int size=sc.nextInt();

		char arr[] = new char[size];

		for(int i=0; i<size; i++){
		
			arr[i]=sc.next().charAt(0);
		}
		for(int i=0; i<size; i++){
			if(arr[i]>='a'&& arr[i]<='z'){
				arr[i]+=(char)-32;
			}	
					
		}
		for(int i=0; i<size; i++){
		
			System.out.println(arr[i]);
		}

	}
}
