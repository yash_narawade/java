/*
 
 Q2.WAP to find the difference between minimum element in an array and maximum
element in an array, take input from the user.
Example :
Input:
Enter the size :
5
Enter the elements of the array:
3
6
9
8
10
Output
The difference between the minimum and maximum elements is: 7
*/

import java.util.*;
class prog2{

	public static void main(String a[]){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter a size");
		int size=sc.nextInt();

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
		
			arr[i]=sc.nextInt();
		}
		int min = arr[0];
		int max = arr[0];
		for(int i=0; i<size; i++){
			
			if(arr[i]<min){
			
				min=arr[i];
			}
			
			if(arr[i]>max){
				max=arr[i];
			}		
		}

		int d = max-min;
		System.out.print("min "+d);
	}
}
