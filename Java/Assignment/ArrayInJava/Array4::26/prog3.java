/*
Q3. WAP to find the second largest element in an array.
Example:
Input:
Enter the size :4
Enter the elements of the array:
1
2
3
4
Output:
The second largest element in the array is: 3
*/
import java.util.*;
class prog3{

	public static void main(String a[]){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter a size");
		int size=sc.nextInt();

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
		
			arr[i]=sc.nextInt();
		}
		int max = arr[0];
		for(int i=0; i<size; i++){
			
			if(arr[i]>max){
			
				max =arr[i];
			}else{
				
			}			
		}

		int sm=arr[0];

		if(arr[0]==max){
			sm=arr[1];
		}

		for(int i=0; i<size; i++){
			if(arr[i]>sm && arr[i]<max){     // 5 4 3 2 1 
				sm=arr[i];
			}
		}

		System.out.println("Second largest : "+sm);

	}
}
