/*
Q1.WAP to take input from the user for size and elements of an array, where you have to
print the average of array elements(Array should be of integers).
Example :
Input:
Enter the size:
4
Enter array elements:
2
4
6
8
Output:
Array elements' average is :5
*/

import java.util.*;
class prog1{

	public static void main(String a[]){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter a size");
		int size=sc.nextInt();

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
		
			arr[i]=sc.nextInt();
		}
		int tot=0;
		for(int i=0; i<size; i++){
		
			tot=tot+arr[i];
		}
		int avg=tot/size;
		System.out.print("Avg "+avg);
	}
}
