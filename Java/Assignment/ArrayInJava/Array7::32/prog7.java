/*
 7.WAP to take numbers from a user in a 2D array and print the product of the primary
diagonal of an array.
Example:
Enter row = 3
Enter column = 3

13 43 70
125 80 26
57 12 16

Output :
Product of Primary Diagonal : 16640

*/
import java.util.*;
class prog7{

	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a array size");
		int size=sc.nextInt();

		int arr[][]=new int[size][size];
		System.out.println("Enter array element");
		
		for(int i=0; i<size; i++){
		
			for(int j=0; j<size; j++){
				
				arr[i][j]=sc.nextInt();			
			}
		}
		int prod=1;
		for(int i=0; i<size; i++){
		
				prod=prod*arr[i][i];
		}	

		System.out.println(prod);
	}
}
