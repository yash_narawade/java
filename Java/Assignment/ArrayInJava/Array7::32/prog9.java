/*
 9.WAP to take numbers from a user in a 2D array and print the product of sum of the
primary and secondary diagonal of an array.
Example:
Enter row = 3

Enter column = 3

13 43 70
125 20 26
17 12 16

Output :
Product of Sum of Primary and Secondary Diagonal : 5243
*/
import java.util.*;
class prog9{

	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a array size");
		int size=sc.nextInt();

		int arr[][]=new int[size][size];
		System.out.println("Enter array element");
		
		for(int i=0; i<size; i++){
 		
			for(int j=0; j<size; j++){
				
				arr[i][j]=sc.nextInt();			
			}
		}
		int sum1=0;
		int sum2=0;
		for(int i=0; i<size; i++){
				sum1=sum1+arr[i][size-i-1];
				sum2=sum2+arr[i][i];
		}	

		System.out.println(sum1*sum2);
	}
}
