/*
 2.WAP to take numbers from a user in a 2D array and print the sum of the array.
Example:
Enter row = 3
Enter column = 3

13 43 70
125 80 26
57 12 16

Output : Sum = 442
 
 */


import java.io.*;
class prog2 {

	public static void main(String args[] )throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a array size");
		int size = Integer.parseInt(br.readLine());

		int arr[][] = new int[size][size];

		System.out.println("Enter a array Element ");
		int sum=0;
		for(int i=0; i<size; i++){
		
			for(int j=0; j<size; j++){
				
				arr[i][j]=Integer.parseInt(br.readLine());
				sum+=arr[i][j];		
			}
		}
			System.out.println(sum);


	}
}
