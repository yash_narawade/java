/*
 1.WAP to take numbers from a user in a 2D array and print the array.
Example:
Enter row = 3
Enter column = 3

12 23 10
45 2 25
87 112 96

Output :
12 23 10
45 2 25
87 112 96

*/

import java.io.*;
class prog1 {

	public static void main(String args[] )throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a array size");
		int size = Integer.parseInt(br.readLine());

		int arr[][] = new int[size][size];

		System.out.println("Enter a array Element ");
		for(int i=0; i<size; i++){
		
			for(int j=0; j<size; j++){
				
				arr[i][j]=Integer.parseInt(br.readLine());		
			}
		}
		for(int i=0; i<size; i++){	
			for(int j=0; j<size; j++){
			
				System.out.print(arr[i][j]+" ");
			}
		
			System.out.println();
		}


	}
}
