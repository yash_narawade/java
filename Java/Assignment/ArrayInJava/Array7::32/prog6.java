/*
 6.WAP to take numbers from a user in a 2D array and print the elements which are
divisible by 3 in an array.

Example:
Enter row = 3
Enter column = 3

33 43 72
125 24 26
17 12 16

Output : 33, 72, 24, 12
 */
import java.io.*;
class prog6{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a array size");
		int size = Integer.parseInt(br.readLine());

		int arr[][]=new int[size][size];
		
		System.out.println("Enter a array element ");
		for(int i=0;i<size;i++){
			
			for(int j=0; j<size; j++){
				arr[i][j]=Integer.parseInt(br.readLine());			
			}
		}
		System.out.println("Eelement are:");
		for(int i=0;i<size;i++){
			
			for(int j=0; j<size; j++){
				if(arr[i][j]%3==0){
					System.out.println(arr[i][j]);
				}
			}
		}



	}
}
