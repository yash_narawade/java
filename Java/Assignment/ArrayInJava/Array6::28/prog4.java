/*
 Q4. You have to take two different 1d arrays of the same size and print the
common elements from the arrays.
arr1
45 67 97 87 90 80

arr2
15 97 67 80 90 10

Output:
Common elements in the given arrays are: 67, 97, 90, 80
 */
import java.util.*;
class prog4{
	public static void main(String []args){
	
		Scanner  sc = new Scanner(System.in);

		System.out.println("Emter a array size ");	
		int size =sc.nextInt();

		int arr1[] = new int[size];
		System.out.println("Enter a array1 Element : ");
		
		for(int i=0; i<size; i++){
			arr1[i]=sc.nextInt();
		}
		
		int arr2[] = new int[size];
		System.out.println("Enter a array2 Element : ");
		
		for(int i=0; i<size; i++){
			arr2[i]=sc.nextInt();
		}
		
		System.out.print("Common elements in the given arrays are: ");
		for(int i=0; i<size; i++){
			for(int j=0; j<size; j++){
			
				if(arr1[i]==arr2[j]){
					
					System.out.print(arr2[j]+" ");

				}
			}
			

		}	
		

	}
}
