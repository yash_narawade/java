/*
Q1. Write a program to check whether the given array is descending or not.
Input 1:
Enter the size of the array:
4
Enter the elements of the array:
15
9
5
1
Output :
Given array is in descending order.
Input 2:
Enter the size of the array:
4
Enter the elements of the array:
1
9
5
1
Output :
Given array is not in descending order.
*/import java.util.*;
class prog1{
	public static void main(String []args){
	
		Scanner  sc = new Scanner(System.in);

		System.out.println("Emter a array size ");	
		int size =sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter a array Element : ");
		
		for(int i=0; i<size; i++){
			arr[i]=sc.nextInt();
		}
		
		int flag=1;
		for(int i=0; i<size; i++){
			for(int j=i+1; j<size; j++){
			
				if(arr[i]<arr[j]){
					flag=0;
				}else{
					flag=1;
				}
			}
		
		}

		if(flag==0){
		
			System.out.println("Asending");
		}else{
			System.out.println("Dsending");
		}

	}
}
