/*
 Q6. Write a program to check whether the array contains element multiple of
user given int value, if yes then print it’s index.
Example :
5 10 16 20 25 30 35 4 8 12 16 20
Input 1:
Enter key : 5
Output:
An element multiple of 5 found at index : 0
An element multiple of 5 found at index : 1
An element multiple of 5 found at index : 3
An element multiple of 5 found at index : 4
An element multiple of 5 found at index : 5
An element multiple of 5 found at index : 6
An element multiple of 5 found at index : 11
Input 2:
Enter key: 99
Output : Element Not Found.
*/
import java.util.*;
class prog6{
	public static void main(String []args){
	
		Scanner  sc = new Scanner(System.in);

		// First array
		
		System.out.println("Emter a array size ");	
		int size =sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter a array Element : ");
		
		for(int i=0; i<size; i++){
			arr[i]=sc.nextInt();
		}
		System.out.print("Enter  a key : ");
		int key = sc.nextInt();

		for(int i=0; i<size; i++){
				if(arr[i]%key==0){
					System.out.println("An element multiple of "+key+" found at index "+i);
				} 
		

		}
	}
}

