/*
 Q9. Write a program to count the palindrome elements in your array.
arr
121 1 58 333 616 9

Count of palindrome elements is : 5

(Single number is also a palindrome number) */
import java.util.*;
class prog9{
	public static void main(String []args){
	
		Scanner  sc = new Scanner(System.in);

		// First array
		
		System.out.println("Emter a array size ");	
		int size =sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter a array Element : ");
		
		for(int i=0; i<size; i++){
			arr[i]=sc.nextInt();
		}

		int cnt=0;
		for(int i=0; i<size; i++){
			int num=arr[i];
			int rev=0;
	
			while(num>0){
			
				int rem =num%10;
				 rev=rev*10+rem;
				num/=10;
			}
			if(rev==arr[i]){
			
				cnt++;
			}
		}

		System.out.print("count of palindrome is"+cnt);

	}
}
