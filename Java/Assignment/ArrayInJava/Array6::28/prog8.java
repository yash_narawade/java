/*
 Q8. Write a program to reverse the char array and print the alternate
elements of the array before and after reverse.

Size = 10
A B C D E F G H I J

After reverse :

J I H G F E D C B A

Output :
Before Reverse:
A C E G I

After Reverse:

J H F D B */
import java.util.*;
class prog8{
	public static void main(String []args){
	
		Scanner  sc = new Scanner(System.in);

		// First array
		
		System.out.println("Emter a array size ");	
		int size =sc.nextInt();

		char arr[] = new char[size];
		System.out.println("Enter a array Element : ");
		
		for(int i=0; i<size; i++){
			arr[i]=sc.next().charAt(0);
		}
		System.out.println("After a reverse ");
		for(int i=0; i<size; i+=2){
		
			System.out.print(arr[i]+" ");
		}
		System.out.println("\nBefore reverse : ");A
		char temp=arr[0];
		for(int i=0; i<size/2; i++){
				
				temp =arr[i];
				arr[i]=arr[size-i-1];
				arr[size-i-1]=temp;
		}
		for(int i=0; i<size; i+=2){
		
			System.out.print(arr[i]+" ");
		}

	}
}
