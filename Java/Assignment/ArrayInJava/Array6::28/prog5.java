/*
 Q5. Take two different arrays where size of array may differ, you have to
create an array by combining both the arrays (you have to merge the arrays)
Example :

arr1
5 10 15 20 25 30 35

arr2
4 8 12 16 20

Array after merger :

5 10 15 20 25 30 35 4 8 12 16 20*/
import java.util.*;
class prog5{
	public static void main(String []args){
	
		Scanner  sc = new Scanner(System.in);

		// First array
		
		System.out.println("Emter a array size ");	
		int size1 =sc.nextInt();

		int arr1[] = new int[size1];
		System.out.println("Enter a array1 Element : ");
		
		for(int i=0; i<size1; i++){
			arr1[i]=sc.nextInt();
		}
		
		// second array		
		
		System.out.println("Emter a array size ");	
		int size2=sc.nextInt();

		int arr2[] = new int[size2];
		System.out.println("Enter a array2 Element : ");
		
		for(int i=0; i<size2; i++){
			arr2[i]=sc.nextInt();
		}

		// mergeed array

		int arr3[] = new int[size1+size2];		
		int n=0;
		for(int i=0; i<size1+size2 ; i++){
				if(i<size1){
					arr3[i]=arr1[i];

				} else{
					
					arr3[i]=arr2[n];
					n++;
				}
			}
			
		for(int i=0; i<size1+size2; i++){
		
			System.out.print(arr3[i]+" ");
		}
		

	}
}

