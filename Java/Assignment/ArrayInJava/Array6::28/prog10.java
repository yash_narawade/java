/*
 * Q10. Write a program to find the third largest element in an array.
Example :
56 15 8 26 7 50 54

Output:
Third largest element is: 50*/



import java.util.*;
class prog10{
	public static void main(String []args){
	
		Scanner  sc = new Scanner(System.in);

		// First array
		
		System.out.println("Emter a array size ");	
		int size =sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter a array Element : ");
		
		for(int i=0; i<size; i++){
			arr[i]=sc.nextInt();
		}
		
				
		int lar=arr[0];			// 56 15 8 26 7 50 54
		for(int i=0; i<size; i++){
			if(arr[i]>lar){
					lar=arr[i];
			}
			
		}

		System.out.println("first largest : "+ lar);
		
		int slar=0;
		if(lar==arr[0]){
		
			slar=arr[1];
		}else{
		
			slar=arr[0];
		}


		for(int i=0; i<size; i++){
		
			if(arr[i]>slar && arr[i]<lar ){   // 56 15 8 26 7 50 54
			
				slar=arr[i];
			}
		}
		
		System.out.println("Second  largest : "+ slar);

		int tlar=0;

		if(arr[0]==lar && arr[1]==slar  || arr[0]==slar && arr[1]==lar){
		
			tlar=arr[2];

		} else if(arr[0]==lar || arr[0]==slar){

			tlar=arr[1];
		} else{

			tlar=arr[0];
		}


		for(int i=0; i<size; i++){
		
			if(arr[i]>tlar && arr[i]<slar){
			
				tlar=arr[i];
			}
		}
		System.out.println("Third largest : "+ tlar);


		System.out.print(tlar);

		
	}
}
