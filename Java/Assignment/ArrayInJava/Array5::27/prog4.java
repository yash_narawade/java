/*
Q4. WAP to check the first duplicate element in an array and return its index.
Example :
Input:
Enter the size of the array:
6
Enter the elements of the array:
1
2
3
3
2
5
*/
import java.util.*;
class prog4{

	public static void main(String []args){
	Scanner sc =new Scanner(System.in);
	System.out.println("Enter size: ");
	int size =sc.nextInt();

	System.out.println("Enter element: ");
	char arr[]= new char[size];

	for(int i=0; i<size; i++){
	
		arr[i]=sc.next().charAt(0);
	}

	for(int i=0; i<size; i++){
		int j=i+1;
		while(j<size){
			if(arr[i]==arr[j]){
				System.out.println("First duplicate element present at index "+i);
				break;
			}
			j++;
		}
	}
    }
}
