
/*Q1. WAP to check whether the array is in ascending order or not.
Input 1:
Enter the size of the array:
4
Enter the elements of the array:
1
5
9
15
Output 1:
The given array is in ascending Order.
*/
import java.util.*;
class prog1{

	public static void main(String []args){
	Scanner sc =new Scanner(System.in);
	System.out.println("Enter size: ");
	int size =sc.nextInt();

	System.out.println("Enter element: ");
	char arr[]= new char[size];

	for(int i=0; i<size; i++){
	
		arr[i]=sc.next().charAt(0);
	}
	
	int flag=0;
	for(int i=0; i<size-1; i++){
		if(arr[i]<arr[i+1]){
			flag=1;
		}
	}

	if(flag==1){
	
		System.out.println("Asecinding ");
	} else{
	
		System.out.println("Decending number ");
	}
	}
}
