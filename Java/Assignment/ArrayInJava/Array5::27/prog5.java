/*
 Q5. WAP to print the count of digits in elements of an array.
Example :
Input:
Enter the size of the array:
4
Enter the elements of the array:
1
225
32
356

Output:
1, 3, 2, 3
*/
import java.util.*;
class prog5{

	public static void main(String []args){
	Scanner sc =new Scanner(System.in);
	System.out.println("Enter size: ");
	int size =sc.nextInt();

	System.out.println("Enter element: ");
	 int arr[]= new int [size];

	for(int i=0; i<size; i++){
	
		arr[i]=sc.nextInt();
	}

	for(int i=0; i<size; i++){
		int num=arr[i];
		int cnt=0;
		while(num>0){
				int rem=num%10;
				cnt++;
				num=num/10;
			}
			System.out.print(cnt+",");

			
		}
	}
    }

