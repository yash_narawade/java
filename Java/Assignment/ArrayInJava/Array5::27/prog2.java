/*
Q2. WAP to print the sum of odd and even numbers in an array.
Enter the size of the array:
6
Enter the elements of the array:
10
15
9
1
12
15
Output:
Odd Sum = 40
Even Sum = 22
*/
import java.io.*;
class prog2{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a size ");
		int size=Integer.parseInt(br.readLine());

		int arr[] =new int[size];

		for(int i=0; i<size; i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}
		int Esum=0;
		int Osum=0;
		for(int i=0; i<size; i++){
			if(arr[i]%2==0){
				Esum+=arr[i];
			}
			else{
				Osum+=arr[i];
			}
		}

		System.out.println("even Sum "+Esum);
		System.out.println("Odd Sum  "+Osum);



	}
}
