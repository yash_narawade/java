/*
Q3. WAP to check if an array is a palindrome or not .
Example :
Input:
Enter the size of the array:
6
Enter the elements of the array:
1
2
3
3
2
1
Output:
The given array is a palindrome array.
*/
import java.io.*;
class prog3{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a size ");
		int size=Integer.parseInt(br.readLine());

		int arr[] =new int[size];

		for(int i=0; i<size; i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}
		int flag=1;
		for(int i=0; i<size/2; i++){
				if(arr[i]==arr[size-i-1]){
					flag=0;
				}

		}
		if(flag==0){
		
			System.out.println("palindrome");
		}
		else{
		
			System.out.println("Not palindrome");
		}
	}
}
