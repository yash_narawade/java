/*
Q9. WAP to take a number from the user and store each element in an array by increasing
the element by one.
Example:
Input:
Enter the Number:
1569872365
Output:
2, 6, 7, 10, 9, 8, 3, 4, 7, 6
Explanation : Each digit in a number is increased by one and stored in an array
*/
import java.util.*;
class prog9{

	public static void main(String []args){
	Scanner sc =new Scanner(System.in);
	System.out.println("Enter size: ");
	int size =sc.nextInt();

	System.out.println("Enter element: ");
	 int arr[]= new int [size];

		for(int i=0; i<size; i++){
	
			arr[i]=sc.nextInt()+1;
		}

		for(int i=0; i<size; i++){
		
			System.out.println(arr[i]);
		}

	}
}
