/*
Q10. WAP to print the factorial of each element in an array.
Example :
Input:
Enter the size of the array:
6
Enter the elements of the array:
1
2
3
5
8
2

Output:
1, 2, 6, 120, 40320, 2
*/
import java.util.*;
class prog10{

	public static void main(String []args){
	Scanner sc =new Scanner(System.in);
	System.out.println("Enter size: ");
	int size =sc.nextInt();

	System.out.println("Enter element: ");
	 int arr[]= new int [size];

		for(int i=0; i<size; i++){
	
			arr[i]=sc.nextInt();
		}
		int fact=1;
		for(int i=0; i<size; i++){
			int num=arr[i];
			for(int j=1; j<=num; j++){
				fact=fact*j;
			}
				System.out.print(fact+",");
				fact=1;
		}

	}
}
