/*
Q7. WAP to find the composite numbers in an array.
Example :
Input:
Enter the size of the array:
6
Enter the elements of the array:
10
22
3
31
50
3

Output:
Composite numbers in an array are: 10, 22, 50 
*/
import java.util.*;
class prog7{

	public static void main(String []args){
	Scanner sc =new Scanner(System.in);
	System.out.println("Enter size: ");
	int size =sc.nextInt();

	System.out.println("Enter element: ");
	 int arr[]= new int [size];

	for(int i=0; i<size; i++){
	
		arr[i]=sc.nextInt();
	}

	System.out.print("Composite number in an array are : ");
	for(int i=0; i<size; i++){
		int temp=1;
		int cnt=0;
		while(temp<=arr[i]){
			if(arr[i]%temp==0){
				cnt++;
			}
			temp++;
		}
			if(cnt>2){
			
				System.out.print(arr[i]+",");
				
			}		
		}
	 }
}
