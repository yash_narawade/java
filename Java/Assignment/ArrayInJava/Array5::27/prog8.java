/*
Q8. WAP to find the second minimum element in an array.
Example:
Input:
Enter the size:5
Enter the elements of the array:
10
2
31

4
0
Output:
The second minimum element in the array is: 2 
*/
import java.util.*;
class prog8{

	public static void main(String []args){
	Scanner sc =new Scanner(System.in);
	System.out.println("Enter size: ");
	int size =sc.nextInt();

	System.out.println("Enter element: ");
	 int arr[]= new int [size];

	for(int i=0; i<size; i++){
	
		arr[i]=sc.nextInt();
	}

	int min=arr[0];

	for(int i=0; i<size; i++){
		for(int j=0; j<size; j++){
		
			if(arr[j]<min){
			min=arr[j];			

			}
		}		
			
	}

	System.out.println("mini "+min);
	
	int min2=0;
	if(min==arr[0]){
	
		min2=arr[1];
	}
	for(int i=0; i<size; i++){
	
		if(arr[i]>min2 && arr[i]<min){
		
			min2=arr[i];
		}
	}

	System.out.println("The second minimum number is "+min);
   }
}
