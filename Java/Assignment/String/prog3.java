/*
 Q3.
WAP to take input as a string from the user and print all the characters one by one.
input : JAVA
Output : 
J
A
V
A
 */
import java.util.*;
class prog3{

	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a String");
		String str1 = sc.next();

		for(int i=0; i<str1.length();i++){
		
			System.out.println(str1.charAt(i));
		}
	}
}
