/*
 Q6.
Write a Java program that compares two strings without considering their case
sensitivity.

Ex:
Str1 = Core2web
Str2 = Core2Web
output=true
 */

import java.util.*;
class prog6{

	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a String");
		String str1 = new String(sc.next());
		
		System.out.println("Enter a String");
		String str2 = new String(sc.next());

		System.out.println(str1.equalsIgnoreCase(str2));


	}
}
