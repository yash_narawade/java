/*
Q10. WAP to check if the string is empty if not then print the last character of the string.
Take string input from the user.
Input: “Core2web”
Output : b
 */

import java.util.*;
class prog10{

	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a String");
		String str1 =sc.next();

		int len=0;
		len=str1.length();
		
		if(str1.isEmpty()){
			System.out.println("String is Empty");
		}
		else{
			System.out.println(str1.charAt(len-1));
		}


	}
}
