/*
10. WAP in notebook & Dry run first then type
Take number of rows from user :
rows = 4

65 B 67 D
B 67 D
67 D
D
rows = 3
A 66 C
66 C
C
*/

import java.io.*;
class prog11{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());  //4
		
	       int temp=0;
		for(int i=1; i<=row; i++){
			
			for(int j=1; j<=row-i+1; j++){
				
				temp=i+j;
				if(temp%2==0){
				System.out.print((j+64 )+" ") ;
				}
				else{
				System.out.print((char)(j+64)+" ");
				}
				temp++;
			}
			
			System.out.println();
		}
	}
}
