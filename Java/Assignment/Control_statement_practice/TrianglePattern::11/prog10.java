/*
9. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
D C B A
D C B
D C
D

Rows = 5
E D C B A
E D C B
E D C
E D
E
*/


import java.io.*;
class prog10{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());  //4
		
	       
		for(int i=1; i<=row; i++){
			int temp=4;
			for(int j=1; j<=row-i+1; j++){
		
				System.out.print((char)(temp+64 )+" ") ;
				temp--;	
			}
			
			System.out.println();
		}
	}
}
