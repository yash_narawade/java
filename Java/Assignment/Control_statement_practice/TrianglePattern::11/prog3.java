/*
2. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
3
3 2

3 2 1

Rows = 4
4
4 3
4 3 2
4 3 2 1
 */


import java.io.*;
class prog3{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());
		for(int i=1; i<=row; i++){
			for(int j=4; j>=i; j--){
		
				System.out.print(j+" ");
			}

		System.out.println();
		}
	}
}
