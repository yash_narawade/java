/*
8. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
1 2 3 4
2 3 4
3 4
4
Rows = 3
1 2 3
2 3
3
*/


import java.io.*;
class prog9{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());  //4
		
	       int temp=1;
		for(int i=1; i<=row; i++){
				
			for(int j=1; j<=row-i+1; j++){
		
				System.out.print(temp++ +" ") ;
				
			}
			
			if(i%2==1){
			  temp=temp-3;
			}

			else{
			
				temp=temp-2;
			}

			
			
					

		System.out.println();
		}
	}
}
