/*
1. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
1
1 2
1 2 3
Rows = 4
1
1 2
1 2 3
1 2 3 4
*/


import java.io.*;
class prog2{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());
		for(int i=1; i<=row; i++){
			for(int j=1; j<=i; j++){
		
				System.out.print(j+" ");
			}

		System.out.println();
		}
	}
}
