/*
5. WAP in notebook & Dry run first then type
Take number of rows from user :
Row = 3
1
2 4
3 6 9

Rows = 4
1
2 4
3 6 9
4 8 12 16
*/


import java.io.*;
class prog6{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());
		
		
		for(int i=1; i<=row; i++){
			
			for(int j=1; j<=i; j++){
		
				System.out.print(i*j +" ");
				
			}
					

		System.out.println();
		}
	}
}
