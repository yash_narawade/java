/*
3. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
A
B C
C D E
Rows = 4
A
B C
C D E
D E F G
*/


import java.io.*;
class prog4{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());
		
		int num=1;	
		for(int i=1; i<=row; i++){
			for(int j=1; j<=i; j++){
		
				System.out.print((char)(num+64)+" ");
				num++;
			}
			
		num=num-i+1;	
			

		System.out.println();
		}
	}
}
