/*
10. WAP in notebook & Dry run first then type
Take number of rows from user:
rows = 4
65 B 67 D
   B 67 D
     67 D
        D

rows = 3
A 66 C
  66 C
     C
   */

import java.io.*;
class prog10{

	public static void main(String [] args)throws IOException{
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	System.out.print("Enter row");
	int row=Integer.parseInt(br.readLine());

	for(int i=1; i<=row; i++){
		for(int j=1; j<i; j++){
		
			System.out.print("\t");
		}
		int ch=64+i;
		for(int k=1; k<=row-i+1; k++){

			if(row%2==0){
				if((i+k)%2==0){
				
					System.out.print(ch+"\t");
				}
				else{
				
					System.out.print((char)ch+"\t");
				}
			}

			else{
			
				if((i+k)%2==0){
				
					System.out.print((char)ch +"\t");
				}

				else{
				
					System.out.print(ch +"\t");
				}
			}
			ch++;
		}

			System.out.println();
	}

	}
}
