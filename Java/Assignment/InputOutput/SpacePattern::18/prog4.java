/*
 * 4. WAP in notebook & Dry run first then type
Take number of rows from user :
Row = 3
    3
  3 6
3 6 9
Rows = 4

        4
      4 8
   4 8 12
4 8 12 16
*/

import java.io.*;
class prog4{

	public static void main(String [] args)throws IOException{
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	System.out.print("Enter row");
	int row=Integer.parseInt(br.readLine());

	for(int i=1; i<=row; i++){
	
		for(int j=1; j<=row-i; j++){
		
			System.out.print("\t");
		}
		int num=row;	
		for(int k=1; k<=i; k++){
		
			System.out.print(k*num +"\t");
		}

			System.out.println();
	}

	}
}
