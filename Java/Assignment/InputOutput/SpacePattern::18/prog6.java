/*
WAP in notebook & Dry run first then type
Take number of rows from user:
Rows = 3
3 3 3
  2 2
    1

Rows = 4
4 4 4 4
  3 3 3
    2 2
      1
   */

import java.io.*;
class prog6{

	public static void main(String [] args)throws IOException{
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	System.out.print("Enter row");
	int row=Integer.parseInt(br.readLine());

	int num=row;
	for(int i=1; i<=row; i++){
		for(int j=1; j<i; j++){
		
			System.out.print("\t");
		}
		for(int k=1; k<=row-i+1; k++){
		
			System.out.print(num+"\t");
		}
		num--;
		System.out.println();
	}

	}
}
