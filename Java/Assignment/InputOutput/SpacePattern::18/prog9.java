/*
WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
D C B A
  D C B
    D C
      D
   */

import java.io.*;
class prog9{

	public static void main(String [] args)throws IOException{
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	System.out.print("Enter row");
	int row=Integer.parseInt(br.readLine());

	for(int i=1; i<=row; i++){
		int num=row+64;
		for(int j=1; j<i; j++){
		
			System.out.print("\t");
		}
		
		for(int k=1; k<=row-i+1; k++){
		
			System.out.print((char)num-- +"\t");
		}

			System.out.println();
	}

	}
}
