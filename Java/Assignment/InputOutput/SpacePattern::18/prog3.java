/*WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
    C
  B C
A B C
*/

import java.io.*;
class prog3{

	public static void main(String [] args)throws IOException{
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	System.out.print("Enter row");
	int row=Integer.parseInt(br.readLine());

	for(int i=1; i<=row; i++){
	
		for(int j=1; j<=row-i; j++){
		
			System.out.print("\t");
		}
		int num=64+row;	
		for(int k=1; k<=i; k++){
		
			System.out.print((char)num-- +"\t");
		}

			System.out.println();
	}

	}
}
