/*
WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
1 2 3 4
  2 3 4
    3 4
      4

Rows = 3
1 2 3
  2 3
    3 
   */

import java.io.*;
class prog8{

	public static void main(String [] args)throws IOException{
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	System.out.print("Enter row");
	int row=Integer.parseInt(br.readLine());

	for(int i=1; i<=row; i++){
		int num=i;
		for(int j=1; j<i; j++){
		
			System.out.print("\t");
		}
		
		for(int k=1; k<=row-i+1; k++){
		
			System.out.print(num++ +"\t");
		}

			System.out.println();
	}

	}
}
