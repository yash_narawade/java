/*WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3

    1
  1 2
1 2 3*/

import java.io.*;
class prog1{

	public static void main(String [] args)throws IOException{
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	System.out.print("Enter row");
	int row=Integer.parseInt(br.readLine());

	for(int i=1; i<=row; i++){
	
		for(int j=1; j<=row-i; j++){
		
			System.out.print("\t");
		}
		
		for(int k=1; k<=i; k++){
		
			System.out.print(k+"\t");
		}

			System.out.println();
	}

	}
}
