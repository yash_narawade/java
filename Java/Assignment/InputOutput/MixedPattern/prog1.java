import java.io.*;
class prog1{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

		int num=1;

		for(int i=1; i<=row; i++){
		
			for(int j=1; j<=row; j++){
			
				System.out.print(num++ +" ");
			}

			System.out.println();
		}

	}
}
