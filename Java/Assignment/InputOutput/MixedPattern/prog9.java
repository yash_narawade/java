import java.io.*;
class prog9{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

		int temp=row-1;	
		for(int i=1; i<=row; i++){
	
			for(int j=1; j<=row-i+1; j++){
				
				if(i%2==1){
					System.out.print(j+" ");
				}	
				else{
				
					System.out.print((char)(64+temp)+" ");
					temp--;
				}	
							
			}
			temp=row-i;		
			System.out.println();
		}

	}
}
