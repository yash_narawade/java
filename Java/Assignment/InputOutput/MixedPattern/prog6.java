import java.io.*;
class prog6{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

		for(int i=1; i<=row; i++){
			
			int temp=row;	
			for(int j=1; j<=i; j++){
				
				if(i%2==1){
					System.out.print((char)(96+temp)+" ");
				}
				else{
					System.out.print(temp+" ");
				}
				temp--;
							
			}

			System.out.println();
		}

	}
}
