import java.io.*;
class prog10{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter a Number");
		long num=Long.parseLong(br.readLine());	//45632985632

		long rem=0l,rev=0l;	
		
		while(num>0){
		
			rem=num%10;
			rev=rev*10+rem;
			num/=10;
		}
		num=rev;
		while(num>0){
		
			rem=num%10;
			if(rem%2==1){
			
				System.out.print(rem*rem+",");
			}
			num/=10;
		}

	}
}
