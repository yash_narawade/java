import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
class Factorial{


	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number");
		int num = Integer.parseInt(br.readLine());
		int fact=1;
		int temp=1;
		while(temp<=num){
		
			fact=fact*temp;
			temp++;
		}

		System.out.println("Factorial of "+num+" is "+fact);
	

	}
}
