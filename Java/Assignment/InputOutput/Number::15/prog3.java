import java.io.*;
class Composite{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter a number");
		int num=Integer.parseInt(br.readLine());
		int temp=1,count=0;


		while(temp<=num){
		
			if(num%temp==0){
			
				count++;
			}
			temp++;
		}

		if(count>2){
		
			System.out.println(num+" is composite");
		}

		else{
		
			System.out.println(num+" is Not composite");
		}

	}
}
