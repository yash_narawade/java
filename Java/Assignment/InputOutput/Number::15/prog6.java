import java.io.*;
class Palindrome{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Number");
		int num=Integer.parseInt(br.readLine());
		int temp=num;		
		int rem=0,rev=0;
		while(num>0){
		
			rem= num%10;
			rev= rev*10+rem;
			num=num/10;
		}

		if(temp==rev){
		
			System.out.println( temp+" Number is Palindrome");
		}

		else{
			System.out.println( temp+" Number is NOT Palindrome");
		}
	}
}
