/*
6.WAP in notebook & Dry run first then type
Take number of rows from user :

row=3

    3
  3 2 3
3 2 1 2 3
row=4

      4
    4 3 4
  4 3 2 3 4
4 3 2 1 2 3 4 
*/


import java.io.*;
class prog7{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

	
		for(int i=1; i<=row; i++){
		
			for(int j=1; j<=row-i; j++){
			
				System.out.print("\t");
			}
			
			int num=row;

			for(int j=1; j<=i*2-1; j++){
				if(j<i){
				
					System.out.print(num-- +"\t");
				} else {
				
					System.out.print(num++ +"\t");
				}
			}
			
			
			System.out.println();



		}

	}
}
