/*
7.WAP in notebook & Dry run first then type
Take number of rows from user :

row=3

    1
  B B B
3 3 3 3 3
row=4

     1
   B B B
 3 3 3 3 3
D D D D D D D
*/


import java.io.*;
class prog8{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

	
		for(int i=1; i<=row; i++){
		
			for(int j=1; j<=row-i; j++){
			
				System.out.print("\t");
			}
			
			for(int j=1; j<=i*2-1; j++){
				
				if(i%2==1){
					System.out.print(i+"\t");
				} else {
				
					System.out.print((char)(64+i)+"\t");
				}
				
			}
			
			
			System.out.println();



		}

	}
}
