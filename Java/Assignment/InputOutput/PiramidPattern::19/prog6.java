/*
5.WAP in notebook & Dry run first then type
Take number of rows from user :
row=3

    1
  1 2 1
1 2 3 2 1
row=4

     1
   1 2 1
 1 2 3 2 1
1 2 3 4 3 2 1 
*/
import java.io.*;
class prog6{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

	
		for(int i=1; i<=row; i++){
		
			for(int j=1; j<=row-i; j++){
			
				System.out.print("\t");
			}
			
			int num=1;
			for(int k=1; k<=i*2-1; k++){
				if(k<i){		
					System.out.print(num++ +"\t");
				}
				else{
					System.out.print((num--)+"\t");
					
				}

			}
				System.out.println("\t");
		}

	}
}
