/*
 2.WAP in notebook & Dry run first then type
Take number of rows from user :
row=3

     1
   2 3 4
 5 6 7 8 9
row=4

	1
      2 3 4
    5 6 7 8 9
 10 11 12 13 14 15 16
*/
import java.io.*;
class prog3{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

		int num=1;
		for(int i=1; i<=row; i++){
		
			for(int j=1; j<=row-i; j++){
			
				System.out.print("\t");
			}
			
			
			for(int k=1; k<=i*2-1; k++){
			
				System.out.print(num++ +"\t");

			}

				System.out.println("\t");
		}

	}
}
