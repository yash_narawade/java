/*
 3.WAP in notebook & Dry run first then type
Take number of rows from user :
row=3

    3
  2 2 2
1 1 1 1 1
row=4

      4
    3 3 3
  2 2 2 2 2
1 1 1 1 1 1 1
*/
import java.io.*;
class prog4{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

		int temp=row;
		for(int i=1; i<=row; i++){
		
			for(int j=1; j<=row-i; j++){
			
				System.out.print("\t");
			}
			
			
			for(int k=1; k<=i*2-1; k++){
			
				System.out.print(temp +"\t");

			}
				temp--;
				System.out.println("\t");
		}

	}
}
