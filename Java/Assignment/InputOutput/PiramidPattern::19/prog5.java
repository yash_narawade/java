/*
 4.WAP in notebook & Dry run first then type
Take number of rows from user :
row=3

    A
  B B B
C C C C C
row=4

     A
   B B B
 C C C C C
D D D D D D D
*/
import java.io.*;
class prog5{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

		char ch='A';
		for(int i=1; i<=row; i++){
		
			for(int j=1; j<=row-i; j++){
			
				System.out.print("\t");
			}
			
			
			for(int k=1; k<=i*2-1; k++){
			
				System.out.print(ch +"\t");

			}
				ch++;
				System.out.println("\t");
		}

	}
}
