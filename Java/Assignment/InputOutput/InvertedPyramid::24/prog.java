/*
 row= 4
* * * * * * *
* * * * *
* * *
*

row=3

* * * * *
* * *
 */
import java.io.*;
class prog{
	public static void main(String [] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row : ");
		int row =Integer.parseInt(br.readLine());

		for(int i=1; i<=row; i++){
			for(int j=1; j<i; j++){
			
				System.out.print("\t");
			}
			for(int j=1; j<=(row-i)*2+1;j++){

				System.out.print("*\t");
			}
			System.out.println();
		}


	}
}
