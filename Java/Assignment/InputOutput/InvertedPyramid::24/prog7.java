/*
7.WAP in notebook & Dry run first then type
Take number of rows from user :
row = 4
A B C D C B A
  A B C B A
    A B A
      A

row = 3

A B C B A
  A B A
    A
*/

import java.io.*;
class prog7{
	public static void main(String [] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row : ");
		int row =Integer.parseInt(br.readLine());
		for(int i=1; i<=row; i++){
		int temp=1;
			for(int j=1; j<i; j++){
			
				System.out.print("\t");
			}
			for(int j=1; j<=(row-i)*2+1;j++){
				if(j<=(row-i)){
					System.out.print((char)(temp+64)+"\t");
					temp++;
				}
				else{
					System.out.print((char)(temp+64)+"\t");
					temp--;
				
				}
				
			}
			System.out.println();
		}


	}
}
