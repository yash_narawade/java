/*
8.WAP in notebook & Dry run first then type
Take number of rows from user :
row=4
1 2 3 4 3 2 1
  2 3 4 3 2
    3 4 3
      4

row=3

1 2 3 2 1
  2 3 2
    3
*/

import java.io.*;
class prog8{
	public static void main(String [] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row : ");
		int row =Integer.parseInt(br.readLine());
		int temp=1;
		for(int i=1; i<=row; i++){
			temp=i;
			for(int j=1; j<i; j++){
			
				System.out.print("\t");
			}
			for(int j=1; j<=(row-i)*2+1;j++){
				if(j<=(row-i)){
					System.out.print(temp++ +"\t");
					
				}
				else{
					System.out.print(temp-- +"\t");
				}
				
			}
			System.out.println();
		}


	}
}
