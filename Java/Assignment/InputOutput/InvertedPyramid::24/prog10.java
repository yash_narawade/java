/*

10.WAP in notebook & Dry run first then type

Take number of rows from user :
rows:3

3 2 1 2 3
  2 1 2
    1

rows:4
4 3 2 1 2 3 4
  3 2 1 2 3
    2 1 2
      1
*/

import java.io.*;
class prog10{
	public static void main(String [] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row : ");
		int row =Integer.parseInt(br.readLine());
		int temp=row;
		for(int i=1; i<=row; i++){
			for(int j=1; j<i; j++){
			
				System.out.print("\t");
			}
			for(int j=1; j<=(row-i)*2+1;j++){
				if(j<=(row-i)){
					System.out.print(temp-- +"\t");
					
				}
				else{
					System.out.print(temp++ +"\t");
				}
				
			}
			temp=row-i;
			System.out.println();
		}


	}
}
