/*
5.WAP in notebook & Dry run first then type
Take number of rows from user :
row =4
A A A A A A A
  B B B B B
    C C C
      D

row = 3

A A A A A
  B B B
    C
*/

import java.io.*;
class prog5{
	public static void main(String [] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row : ");
		int row =Integer.parseInt(br.readLine());
		int temp=1;
		for(int i=1; i<=row; i++){
			for(int j=1; j<i; j++){
			
				System.out.print("\t");
			}
			for(int j=1; j<=(row-i)*2+1;j++){

				System.out.print((char)(temp+64)+"\t");
				
			}
			temp++;
			System.out.println();
		}


	}
}
