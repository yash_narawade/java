/*
9.WAP in notebook & Dry run first then type
Take number of rows from user :
row=3

1 0 1 0 1
  1 0 1
    1

row = 4
1 0 1 0 1 0 1
  1 0 1 0 1
    1 0 1
      1
*/

import java.io.*;
class prog9{
	public static void main(String [] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row : ");
		int row =Integer.parseInt(br.readLine());
		for(int i=1; i<=row; i++){
		int temp=1;
			for(int j=1; j<i; j++){
			
				System.out.print("\t");
			}
			for(int j=1; j<=(row-i)*2+1;j++){
				if(j%2==1){
					System.out.print(temp-- +"\t");
					
				}
				else{
					System.out.print(temp++ +"\t");
				}
				
			}
			System.out.println();
		}


	}
}
