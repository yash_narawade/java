/*
Q8. Take row input from the user.
row=4

      A
    B A B
  C B A B C
D C B A B C D
  C B A B C
    B A B
      A

row=3

    A
  B A B
C B A B C
  B A B
    A
*/


import java.io.*;
class prog8{

        public static void main(String [] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter a row");
                int row = Integer.parseInt(br.readLine());

                int sp=0;
                int col1=0;

                char ch='A';
                for(int i=1; i<row*row; i++){

                        if(i<=row){

                                sp=row-i;
                                col1=i*2-1;

                        }
                        else{
                                sp=i-row;
                                col1=col1-2; 
				ch=(char)(row*2-i+64);                         

                        }

                        for(int j=1; j<=sp; j++){

                                System.out.print("\t");
                        }


                        for(int j=1; j<=col1; j++){
			
				if(j<=col1/2){
					System.out.print(ch--+"\t");
				}  else{
					System.out.print(ch+++"\t");
				
				}
                        }

                        System.out.println();
                }
        }
}

