/*
Q5. Take row input from the user.
row = 4

      4
    3 3 3
  2 2 2 2 2
1 1 1 1 1 1 1
  2 2 2 2 2
    3 3 3
      4

row=3

    3
  2 2 2
1 1 1 1 1
  2 2 2
    3
*/

import java.io.*;
class prog5{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a row");
		int row = Integer.parseInt(br.readLine());
		
		int sp=0;
		int col1=0;

		int num=1;
		for(int i=1; i<row*row; i++){
		
			if(i<=row){
			
				sp=row-i;
				col1=i*2-1;
				num=row-i+1;
			}
			else{
				sp=i-row;
				col1=col1-2;
				num++;
			}

			for(int j=1; j<=sp; j++){
			
				System.out.print("\t");
			}
			
			
			for(int j=1; j<=col1; j++){
				
				System.out.print(num+"\t");
			}

			System.out.println();
		}
	}
}
