/*
		WRONG CODE
   Q10. Take row input from the user.
row=4

      1
    1 A 1
  1 A 2 A 1
1 A 2 B 2 A 1
  1 A 2 A 1
    1 A 1
      1

row=3

    1
  1 A 1
1 A 2 A 1
  1 A 1
    1
 */
import java.util.*;


class Diamond {

        public static void main(String args[]) {

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter row");
                int row = sc.nextInt();

                int col = 0;
                int sp  = 0;
		int n = 0 ;

                for( int i = 1; i <= 2*row-1; i++ ) {

                        if( i <= row ) {

                                sp = row - i;
                                col = 2 * i - 1;
				n = 1;

                        } else {

                                sp = i - row;
                                col = col - 2;
				n = 1;
                        }

                        for( int j = 1; j <= sp; j++) {

                                System.out.print("\t");
                        }

			for( int j = 1; j <= col; j++ ) {

				if( j < col / 2 + 1 ) {

					if( j % 2 == 1 ) {

						System.out.print(n+"\t");

					} else {

						System.out.print((char)(64+n)+"\t");
						n++;
					}

				} else {

					if( j % 2 == 1 ) {

                                                System.out.print(n+"\t");
						n--;

                                        } else {

                                                System.out.print((char)(64+n)+"\t");
                                        }
				}

			}


                        System.out.println();

                }


        }
}

