/*
 Q2.Take row input from the user.
row = 4

         1
      2  3  4
    5 6  7  8  9
10 11 12 13 14 15 16
   18 19 20 21 22
      23 24 25
         26

row = 3

    1
  2 3 4
5 6 7 8 9
 10 11 12
    13
 */

import java.io.*;
class prog2{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a row");
		int row = Integer.parseInt(br.readLine());
		
		int col=0;
		int col1=0;
		int num=1;
		for(int i=1; i<row*row; i++){
		
			if(i<=row){
			
				col=row-i;
				col1=i*2-1;
			}
			else{
				col=i-row;
				col1=col1-2;
			}

			for(int j=1; j<=col; j++){
			
				System.out.print("\t");
			}

			for(int j=1; j<=col1; j++){
			
				System.out.print(num++ +"\t");
			}

			System.out.println();
		}
	}
}
