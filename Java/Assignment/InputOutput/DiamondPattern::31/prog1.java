/*
Q1.Take row input from the user.
row=4

      #
    # # #
  # # # # #
# # # # # # #
  # # # # #
    # # #
      #

row=3

    #
  # # #
# # # # #
  # # #
    #
*/

import java.io.*;
class prog1{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a row");
		int row = Integer.parseInt(br.readLine());
		
		int col=0;
		int col1=0;
		for(int i=1; i<row*row; i++){
		
			if(i<=row){
			
				col=row-i;
				col1=i*2-1;
			}
			else{
				col=i-row;
				col1=col1-2;
			}

			for(int j=1; j<=col; j++){
			
				System.out.print("\t");
			}
			
			
			for(int j=1; j<=col1; j++){

				System.out.print("#\t");
			}

			System.out.println();
		}
	}
}
