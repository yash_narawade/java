import java.io.*;
class prog9{

	public static void main(String []args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a number of row");
		int row=Integer.parseInt(br.readLine());

		int temp=99;
		for(int i=1; i<=row; i++){
			int num=1;
	 		for(int j=1; j<=i; j++){
				
				if(j%2==1){
				
					System.out.print(num+" ");

				}

				else{
				
					System.out.print((char)(temp)+" ");
					temp+=2;
				}
				num++;
				
				
			}
				
		System.out.println();
		}
	


	}
}
