import java.io.*;
class prog7{

	public static void main(String []args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a number of row");
		int row=Integer.parseInt(br.readLine());

		int temp=1;
		for(int i=1; i<=row; i++){
		
	 		for(int j=1; j<=i; j++){
				
				if(i%2==1){
				
					System.out.print(j+" ");
				}

				else{
				
					System.out.print((char)(64+temp)+" ");
				}
				temp++;
				
			}
				
		System.out.println();
		}
	


	}
}
