/*
10. WAP to check whether the given number is an armstrong number or not.
Input 1:
Enter a number: 153
Output 1 :
153 is an Armstrong Number.
Input 2:
Enter a number: 120
Output 2 :

120 is not an Armstrong Number.

Armstrong number
it's a number where the sum of its own digits, each raised to the power of the number of digits, equals the
number itself.
For example, let's take the number 153:
The number of digits in 153 is 3.
1^3+5^3+3^3=1+125+27=153
153 is an armstrong number.
*/
import java.util.*;
class prog10{

	public static void main(String [] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a number");
		int num=sc.nextInt();  // 153
		int temp=num;
		int rem=1;
		int sum=1;
		int cnt=0;
	int v = num;
		int var = 0;
		while(num>0){
			
			num/=10;
			cnt++;
			
		}
		num=temp;
		while(num>0){
				
			rem=num%10;
			for(int i=1; i<=cnt; i++){
				sum=sum*rem;
			}
				var +=sum;
			num/=10;

		}

		if(v==var){
		
			System.out.println("Armstrong Number");
		}else{
			System.out.println(" Not Armstrong Number");
		}

	
	
	}
}
