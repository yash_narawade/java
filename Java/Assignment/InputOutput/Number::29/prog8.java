/*
8. WAP to check Whether a number is a Duck Number or not.
Input 1:
Enter a number: 101150
Output 1 :
101150 is a Duck Number.
Input 2:

Enter a number: 82569
Output 2 :
82569 is not a Duck Number.


Duck Number
A Duck number is any number that contains the digit zero but doesn't start with it.
102 is a Duck number because it contains the digit zero and doesn't start with it.
*/
import java.util.*;
class prog8{

	public static void main(String [] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a number");
		int num=sc.nextInt();  
		boolean isduck=false;
		while(num>0){
			
			int rem=num%10;
			if(rem==0){
				isduck=true;
				break;
			}
			num/=10;
			
		}

		if(isduck){
		
			System.out.println("Duck Number");
		}else{
			System.out.println(" Not Duck Number");
		}

	
	
	}
}
