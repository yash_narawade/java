/*
7. WAP to check whether the given number is a Happy number or not.
Input 1:
Enter a number: 10
Output 1 :
10 is a Happy Number.
Input 2:
Enter a number: 12
Output 2 :
12 is not a Happy Number.

Happy number
A happy number is a positive integer that, when iteratively replaced by the sum of the squares of its digits,
eventually reaches 1.
Let’s Consider a number 10:
1^2 + 0^2 = 1
It’s a happy number.
*/
import java.util.*;
class prog7{

	public static void main(String [] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a number");
		int num=sc.nextInt();  // 43
		int sum=0;
		while(num>9){
			sum=0;
			while(num>0){
			
				int rem=num%10;
				sum+=(rem*rem);
				num/=10;
			
			}
			num=sum;
		}
			if(sum==1){
		
				System.out.println("Happy Number");
			}else{
				System.out.println(" Not Happy Number");
			}
	
	}
}
