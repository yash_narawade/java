/*
5. WAP to check whether the given number is Automorphic or not.
Input 1:
Enter a number: 25
Output 1 :
25 is an Automorphic Number.
Input 2:
Enter a number: 4
Output 2 :
4 is not an Automorphic Number.

Automorphic Number
A number is automorphic if its square ends with the number itself.
The square of 25 is 625.
Since 25 appears at the end of 625, 25 is an automorphic number.
*/
import java.util.*;
class prog5{

	public static void main(String [] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a number");
		int num=sc.nextInt();  // 5
		
		int sq=1;
		sq=num*num;		
		int rem=sq%10;
		if(rem==num){
		
			System.out.println("Automorphic Number");
		}else{
			System.out.println("Not Automorphic Number");
		}
	
	
	
	}
}
