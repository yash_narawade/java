/*
 1. WAP to check whether the given number is a Perfect number or not.
Input 1:
Enter a number: 6
Output 1 :
6 is a Perfect Number.

Input 2:
Enter a number: 15
Output 2 :
15 is not a Perfect Number.

Perfect Number:
A perfect number is a positive integer that is equal to the sum of its proper divisors, excluding itself. The
simplest way to understand this is by looking at the smallest perfect number, which is 6. The divisors of 6
(excluding itself) are 1, 2, and 3, and if you add these divisors together (1 + 2 + 3), the sum is 6.
*/
import java.util.*;
class prog1{

	public static void main(String [] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a number");
		int num=sc.nextInt();
		
		int add=0;
		int temp=1;
		while(temp<num){
		
			if(num%temp==0){
				
				add=add+temp;
			}temp++;
		}

		if(add==num){
		
			System.out.println("Perfect");
		}else{
			System.out.println("Not Perfect");
		}
	
	
	
	}
}
