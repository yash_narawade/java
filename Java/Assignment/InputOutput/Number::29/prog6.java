/*
6. WAP that generates the first n numbers in the Fibonacci series.
Input :
Enter a number: 10
Output :
Fibonacci Series : 0, 1, 1, 2, 3, 5, 8, 13, 21, 34
Fibonacci series

The Fibonacci series is a sequence of numbers where each number is the sum of the two preceding ones,
usually starting with 0 and 1. In mathematical terms, the Fibonacci sequence
F(n) is defined by the recurrence relation:
F(n)=F(n−1)+F(n−2)
with initial conditions
F(0)=0 and
F(1)=1.
So, the Fibonacci sequence starts like this:
0,1,1,2,3,5,8,13,21,34,...
*/
import java.util.*;
class prog6{

	public static void main(String [] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a number");
		int num=sc.nextInt();  // 10
		
		int first=0;
		int second=1;
		for(int i=1; i<=num;i++){
			System.out.print(first+",");
			int sum =first+second;
			first=second;
			second=sum;
		}

		

	
	
	}
}
