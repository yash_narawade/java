/*
4. WAP to check whether the given number is an Abundant number or not.
Input 1:
Enter a number: 25
Output 1 :
25 is an Abundant Number.

Input 2:
Enter a number: 12

Output 2 :
12 is not an Abundant Number.


Abundant Number:
An abundant number is a positive integer that is smaller than the sum of its proper divisors, excluding
itself. In other words, the sum of the proper divisors of an abundant number is greater than the number
itself.
let's consider the number 12:
The proper divisors of 12 are 1, 2, 3, 4, and 6.
The sum of these proper divisors is 1 + 2 + 3 + 4 + 6 = 16.
Since 16 is greater than 12, 12 is an abundant number. 
*/
import java.util.*;
class prog4{

	public static void main(String [] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a number");
		int num=sc.nextInt();  // 21
		
		int add=0;

		for(int i=1; i<num; i++){
			if(num%i==0){
				add+=i;
			}
		}
		if(add>num){
		
			System.out.println("Abundent Number");
		}else{
			System.out.println("Not Abundent Number");
		}
	
	
	
	}
}
