import java.io.*;
class prog2{

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());
		
		int temp=2;
		for(int i=1; i<=row; i++){
		
			for(int j=1; j<=row-i+1; j++){
			
				System.out.print(temp +" ");
				temp+=2;
			}
			
			System.out.println();
		}
	

	}
}
