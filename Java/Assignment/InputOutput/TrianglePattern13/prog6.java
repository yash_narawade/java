import java.io.*;
class prog6{

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());		
		
		
		for(int i=1; i<=row; i++){
			char ch='a';
			int temp=1;
			for(int j=1; j<=row-i+1; j++){
				
				if(j%2==1){
					System.out.print(temp +" ");
					temp++;
				}

				else{
					System.out.print(ch++ +" ");
				}
				
			}
			
			System.out.println();
		}
	

	}
}
