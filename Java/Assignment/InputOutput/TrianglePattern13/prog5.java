import java.io.*;
class prog5{

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());		

		for(int i=1; i<=row; i++){
			
			for(int j=1; j<=row-i+1; j++){
				
				if(i%2==1){
					System.out.print((char)(64+j) +" ");
				}

				else{
					System.out.print((char)(96+j)+" ");
				}
				
			}
			
			System.out.println();
		}
	

	}
}
