import java.io.*;
class prog7{

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());		
		
		int ch=row;	
		for(int i=1; i<=row; i++){
			for(int j=1; j<=row-i+1; j++){
				
				if(j%2==1){
					System.out.print(ch +" ");
					
				}

				else{
					System.out.print((char)(ch+96) +" ");
				}
				ch--;				
			}
			ch=row;
			ch=ch-i;			
			System.out.println();
		}
	

	}
}
