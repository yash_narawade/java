/*
 2. WAP in notebook & Dry run first then type
Take the number of rows from the user :
row=3
c d E
f G H
I J K
row=4
d e f G
h i J K
l M N O
P Q R S
 */
import java.io.*;
class prog2{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

		int temp=row;
		for(int i=1; i<=row; i++){
		
			for(int j=1; j<=row; j++){
				
				if(j<=(row*i)-i){
				
					System.out.print((char)(temp+96)+"\t");
				}
				else{
				
					System.out.print((char)(temp+64)+"\t");
				}
				temp++;
			}
					System.out.println();
		}
	
	}
}
