/*
 5. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
3 $ 4
4 4 4
4 $ 5
row=4
4 $ 5 $
6 6 6 6
6 $ 7 $
8 8 8 8
 */
import java.io.*;
class prog5{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

		int temp=row;
		for(int i=1; i<=row; i++){
		
			for(int j=1; j<=row; j++){
				
				if(i%2==1){
					if(j%2==1 ){
						System.out.print(temp++ +" ");
						
					}else{
					
						System.out.print("$"+" ");
					}

					if(j==row &&j%2==1 ){
						temp--;
					}
					
				} else{
					System.out.print(temp+" ");
				}			
			}
					System.out.println();
		}
	
	}
}
