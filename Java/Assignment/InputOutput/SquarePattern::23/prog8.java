/*
8. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
8 e 24
g h i
80 k 120
row=4
15 f 35 h
i j k l
143 n 195 p
q r s t 
 */
import java.io.*;
class prog8{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

		int temp=row;
		for(int i=1; i<=row; i++){
			for(int j=1; j<=row; j++){
				if(i%2==1){
					if(j%2==1){
				
						System.out.print((temp*temp)-1+"\t");
					}
					else{
				
						System.out.print((char)(temp+97)+"\t");
					}
				}else{
						System.out.print((char)(temp+97)+"\t");
				

				}
					temp++;
				
			}
					System.out.println();
		}
	
	}
}
