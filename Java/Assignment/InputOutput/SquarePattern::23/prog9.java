/*
9. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
9 @ 7
12 @ 8
9 @ 3
row=4
16 @ 14 @
24 @ 20 @
24 @ 18 @
16 @ 8 @ 
 */
import java.io.*;
class prog9{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

		int temp=row;
		for(int i=1; i<=row; i++){
			for(int j=1; j<=row; j++){
				if(i%2==1){
					if(j%2==1){
				
						System.out.print((temp*temp)-1+"\t");
					}
					else{
				
						System.out.print((char)(temp+97)+"\t");
					}
				}else{
						System.out.print((char)(temp+97)+"\t");
				

				}
					temp++;
				
			}
					System.out.println();
		}
	
	}
}
