/*
 7. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
8 16 24
36 49 64
80 100 120

row=4
15 25 35 49
64 81 100 121
143 169 195 225
256 289 324 361 
 */
import java.io.*;
class prog7{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

		int temp=row;
		for(int i=1; i<=row; i++){
			for(int j=1; j<=row; j++){
				if(i%2==1){
					if(j%2==1){
				
						System.out.print((temp*temp)-1+"\t");
					}
					else{
				
						System.out.print(temp*temp+"\t");
					}
				}else{
						System.out.print(temp*temp+"\t");
				

				}
					temp++;
				
			}
					System.out.println();
		}
	
	}
}
