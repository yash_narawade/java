/*
 4. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3

& & &
& $ &
& & &
row=4
& & & &
& $ & $
& & & &
& $ & $
 */
import java.io.*;
class prog4{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

		
		for(int i=1; i<=row; i++){
		
			for(int j=1; j<=row; j++){
				
				if(i%2==1){
					System.out.print("& ");
					
				} else{
					if(j%2==1){
						System.out.print("& ");
					} else {
						System.out.print("$ ");
					}
				}

			}
					System.out.println();
		}
	
	}
}
