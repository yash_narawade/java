/*
1. WAP in notebook & Dry run first then type
Take a number of rows from the user :
row=3
C d e
F g h
I j k
row=4
D e f g
H i j k
L m n o
P q r s
*/
import java.io.*;
class prog1{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

		int temp=row;
		for(int i=1; i<=row; i++){
		
			for(int j=1; j<=row; j++){
				
				if(j==1){
					System.out.print((char)(temp+64)+" ");
					
				} else{
									
					System.out.print((char)(temp+96)+" ");
				}
				temp++;
			}
					System.out.println();
		}
	
	}
}
