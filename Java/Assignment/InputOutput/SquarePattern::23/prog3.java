/*
 3. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
c 4 e
6 g 8
i 10 k
row=4
d 5 f 7
8 i 10 k
l 13 n 15
16 q 18 s
 */
import java.io.*;
class prog3{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row=Integer.parseInt(br.readLine());

		int temp=row;
		for(int i=1; i<=row; i++){
		
			for(int j=1; j<=row; j++){
				
				if((i+j)%2==0){
					System.out.print((char)(temp+96)+" ");
					
				} else{
									
					System.out.print(temp+" ");
				}
				temp++;
			}
					System.out.println();
		}
	
	}
}
