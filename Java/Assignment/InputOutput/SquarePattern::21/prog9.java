/*
9. WAP in notebook & Dry run first then type
Take number of rows from user :

row=3
2 6 6
3 4 9
2 6 6

row=4
2 6 6 12
3 4 9 8
2 6 6 12
3 4 9 8
*/

import java.io.*;
class prog9{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row= Integer.parseInt(br.readLine());

	
		int temp=row;
		for(int i=1; i<=row; i++){
	
			for(int j=1; j<=row; j++){
				if(i%2==1){
					if(j%2==1){
						System.out.print(j*2+" ");
					}
					else{
				
						System.out.print(j*3+" ");
					}
				
				}

				else{
				
					if(j%2==1){
						System.out.print(j*3+" ");
					}

					else{
						System.out.print(j*2+" ");
					}
				}
			
			
		}		

		
			
			System.out.println();
		}


	}
}
