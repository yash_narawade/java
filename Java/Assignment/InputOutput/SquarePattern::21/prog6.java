/*
6. WAP in notebook & Dry run first then type
Take number of rows from user :

row=3
A 4 A
6 B 8
C 10 C

row=4
4 A 6 A
8 B 10 B
12 C 14 C
16 D 18 D
*/

import java.io.*;
class prog6{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row= Integer.parseInt(br.readLine());

		int temp=row;
		char ch='A';
		for(int i=1; i<=row; i++){
			for(int j=1; j<=row; j++){
			
				if(temp%2==0){
					System.out.print(temp+" ");
				} else{
					System.out.print(ch+" ");
				}

				temp++;

			}
			ch++;
			System.out.println();
		}


	}
}
