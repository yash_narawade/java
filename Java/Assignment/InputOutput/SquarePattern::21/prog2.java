/*
 2. WAP in notebook & Dry run first then type
Take number of rows from user :

row=3
9 4 25
18 7 8
27 50 11

row=4

4 25 18 7
8 27 50 11
36 13 14 45
16 17 54 19

*/import java.io.*;
class prog2{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row= Integer.parseInt(br.readLine());

		int temp=row;
		for(int i=1; i<=row; i++){
			for(int j=1; j<=row; j++){
			
				if(temp%3==0){
					System.out.print(temp*3+" ");
				} else if(temp%5==0){
					System.out.print(temp*5+" ");
				} else{
					System.out.print(temp+" ");
				}

				temp++;

			}
			System.out.println();
		}


	}
}
