/*
8. WAP in notebook & Dry run first then type
Take number of rows from user :

row=3
# C #
C # B
# C #

row=4
# D # C
D # C #
# D # C
D # C #
*/

import java.io.*;
class prog8{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row= Integer.parseInt(br.readLine());

	
		
		for(int i=1; i<=row; i++){
			char ch=(char)(row+64);
			for(int j=1; j<=row; j++){
			
				if((i+j)%2==0){
					System.out.print("# ");
				} else{
					System.out.print(ch-- +" ");
				}

			

			}
			
			System.out.println();
		}


	}
}
