/*
 1. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
C B A
3 3 3
C B A

row=4
D C B A
4 4 4 4
D C B A
4 4 4 4

*/import java.io.*;
class prog1{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row= Integer.parseInt(br.readLine());

		for(int i=1; i<=row; i++){
			int temp=row;
			for(int j=1; j<=row; j++){
			
				if(i%2==0){
					System.out.print(row+" ");
				} else{
					
					System.out.print((char)(temp+64)+" ");
				}
				temp--;

			}
			System.out.println();
		}


	}
}
