/*
 8.WAP in notebook & Dry run first then type
Take number of rows from user :

row=3

    3
  2 3 4
1 2 3 4 5

row=4

     4
   3 4 5
 2 3 4 5 6
1 2 3 4 5 6 7
 */

import java.io.*;
class prog8{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter  a row");
		int row=Integer.parseInt(br.readLine());
	
		int temp=row;
		for(int i=1; i<=row; i++){
			for(int j=1; j<=row-i; j++){
				System.out.print("\t");
			}
			for(int j=1; j<=i*2-1; j++){
				System.out.print(temp++ +"\t");
			}
				temp=row-i;
				System.out.println();
			
		}
	
	}

}
