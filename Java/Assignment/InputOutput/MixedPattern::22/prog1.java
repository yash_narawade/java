import java.io.*;
class prog1{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter  a row");
		int row=Integer.parseInt(br.readLine());
	
		int temp=1;
		for(int i=1; i<=row; i++){

			for(int j=1; j<=row-i; j++){
			
				System.out.print("\t ");
			}
		
			for(int j=1; j<=i; j++){
				
				System.out.print( temp+"\t ");
				temp+=2;
			}
				System.out.println();
		}
	
	}

}
