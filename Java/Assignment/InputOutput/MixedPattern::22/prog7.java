/*
 7.WAP in notebook & Dry run first then type
Take number of rows from user :

row=3

    1
  3 2 1
5 4 3 2 1
row=4

     1
   3 2 1
 5 4 3 2 1
7 6 5 4 3 2 1
 */

import java.io.*;
class prog7{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter  a row");
		int row=Integer.parseInt(br.readLine());
	
		int temp=1;
		for(int i=1; i<=row; i++){
			for(int j=1; j<=row-i; j++){
				System.out.print("\t");
			}
			int num=i*2-1;
			for(int j=1; j<=i*2-1; j++){
				System.out.print(num--+"\t");
			}
				
				System.out.println();
			
		}
	
	}

}
