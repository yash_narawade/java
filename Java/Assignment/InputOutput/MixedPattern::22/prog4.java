/*
 4. WAP in notebook & Dry run first then type
Take number of rows from user : (check the row is even or odd if odd print Capital
letters else print small letters)
row=3
A B C
  B C
    C

row=4
a b c d
  b c d
    c d
      d
 */

import java.io.*;
class prog4{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter  a row");
		int row=Integer.parseInt(br.readLine());
	
		int temp=1;
		for(int i=1; i<=row; i++){
			for(int j=1; j<i;j++){
			
				System.out.print("\t");
			}

			for(int j=1; j<=row-i+1; j++){
				
				if(row%2==1){
					System.out.print((char)(temp+64) +"\t");
				} else{
					System.out.print((char)(temp+96)+"\t");
				}
				temp++;
			}
				temp-=(row-i);
				System.out.println();
			
		}
	
	}

}
