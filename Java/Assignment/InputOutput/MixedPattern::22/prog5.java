/*
 5. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
9 16 25
6 7 8
81 100 121
row=4
9 16 25 36
7 8 9 10
121 144 169 225
16 17 18 19
 */

import java.io.*;
class prog5{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter  a row");
		int row=Integer.parseInt(br.readLine());
	
		int temp=3;
		for(int i=1; i<=row; i++){
			for(int j=1; j<=row; j++){
					if(temp%14==0){temp++;}
					if(i%2==1){
						System.out.print(temp*temp+"\t");
					} else{
						System.out.print(temp+"\t");
					}
				
				
				temp++;
			}
				
				System.out.println();
			
		}
	
	}

}
