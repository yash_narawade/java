/*
 2. WAP in notebook & Dry run first then type
Take number of rows from user :
row = 3
1 2 3
  3 4
    4
row =4
1 2 3 4
  4 5 6
    6 7
      7
 */
import java.io.*;
class prog2{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter  a row");
		int row=Integer.parseInt(br.readLine());
	
		int temp=1;
		for(int i=1; i<=row; i++){
			for(int j=1; j<i;j++){
			
				System.out.print("\t");
			}

			for(int j=1; j<=row-i+1; j++){
			
				System.out.print(temp++ +"\t");
			}
				temp--;
				System.out.println();
			
		}
	
	}

}
