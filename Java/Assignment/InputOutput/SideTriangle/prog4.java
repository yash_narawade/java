/*
Q4.Take row input from the user.
row = 4
4
3 3
2 2 2
1 1 1 1
2 2 2
3 3
4
row=3
3
2 2
1 1 1
2 2
3
*/

import java.io.*;
class prog4{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row = Integer.parseInt(br.readLine());

		int num=row;
		int col=0;
		for(int i=1; i<row*2; i++){
		
			if(i<=row){
				col=i;
				num=row-i+1;	
			} else{
			
				col=row*2-i;
				num++;
			}
			for(int j=1; j<=col; j++){
			
				System.out.print(num +"\t");
			}

			
			
		System.out.println();
		}

	}
}
