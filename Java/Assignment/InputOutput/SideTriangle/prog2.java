/*
Q2.Take row input from the user.
row = 4
1
1 2
1 2 3
1 2 3 4
1 2 3
1 2
1

row = 3
1
1 2
1 2 3
1 2
1
 */
import java.io.*;
class prog2{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row = Integer.parseInt(br.readLine());

		int col=0;
		for(int i=1; i<row*2; i++){
		
			if(i<=row){
				col=i;	
			} else{
			
				col=row*2-i;
			}
			for(int j=1; j<=col; j++){
			
				System.out.print(j+"\t");
			}
		System.out.println();
		}

	}
}
