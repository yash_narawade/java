/*
Q7.Take row input from the user.
row=4

      1
    2 1
  3 2 1
4 3 2 1
  3 2 1
    2 1
      1

row=3

    1
  2 1
3 2 1
  2 1
    1

*/

import java.io.*;
class prog7{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row = Integer.parseInt(br.readLine());
		
		int num=0;
		int col=0;
		int col1=0;
		for(int i=1; i<row*2; i++){
			
			if(i<=row){
				col=row-i;
				col1=i;
				num=i;
			} else{
			
				col=i-row;
				col1=row*2-i;
				num=row*2-i;
			}

			for(int j=1; j<=col;j++){
				System.out.print("\t");
			}

			for(int j=1; j<=col1; j++){
			
				System.out.print(num--+"\t");
			}

			
			
		System.out.println();
		}

	}
}
