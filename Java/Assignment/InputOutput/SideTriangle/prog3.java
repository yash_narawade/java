/*
Q3.Take row input from the user.
row=3
1
2 1
3 2 1
2 1
1
row=4
1
2 1
3 2 1
4 3 2 1
3 2 1
2 1
1
 */
import java.io.*;
class prog3{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row = Integer.parseInt(br.readLine());

		int col=0;
		for(int i=1; i<row*2; i++){
		
			if(i<=row){
				col=i;	
			} else{
			
				col=row*2-i;
			}
			int num=col;
			for(int j=1; j<=col; j++){
			
				System.out.print(num-- +"\t");
			}
		System.out.println();
		}

	}
}
