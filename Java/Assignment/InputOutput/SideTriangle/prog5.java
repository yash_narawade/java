/*
Q5.Take row input from the user.
row=4
D
C C
B B B
A A A A
B B B
C C
D

row=3
C
B B
A A A
B B
C
*/

import java.io.*;
class prog5{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row = Integer.parseInt(br.readLine());

		int num=row;
		int col=0;
		for(int i=1; i<row*2; i++){
		
			if(i<=row){
				col=i;
				num=row-i+1;	
			} else{
			
				col=row*2-i;
				num++;
			}
			for(int j=1; j<=col; j++){
			
				System.out.print((char)(num+64) +"\t");
			}

			
			
		System.out.println();
		}

	}
}
