/*
Q10.Take row input from the user.
row=4

      D
    C D
  B C D
A B C D
  B C D
    C D
      D

row=3

    C
  B C
A B C
  B C
    C
*/

import java.io.*;
class prog10{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a row");
		int row = Integer.parseInt(br.readLine());
		
		int num=0;
		int col=0;
		int col1=0;
		for(int i=1; i<row*2; i++){
			
			if(i<=row){
				col=row-i;
				col1=i;
				num=row-i;
			} else{
			
				col=i-row;
				col1=row*2-i;
				num=i-row;
			}

			for(int j=1; j<=col;j++){
				System.out.print("\t");
			}

			for(int j=1; j<=col1; j++){
			
				System.out.print((char)(num+65) +"\t");
				num++;
			}
		System.out.println();
		}

	}
}
