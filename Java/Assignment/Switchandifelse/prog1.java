class Oddoreven{

	public static void main(String [] args){
	
		int num=45;
	
		switch(num%2){
		
			case 0 :
				System.out.println("Even Number");
				break;

			case 1 :
				System.out.println("Odd Number");
				break;

			default:

				System.out.println("Invalid Number");
		}



		// If else code
		

		if(num%2==0){
		
			 System.out.println("Even Number");

		}
		else{
		
			 System.out.println("Odd Number");

		}
	}
}
