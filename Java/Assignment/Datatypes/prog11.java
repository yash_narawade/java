/*
11. Write a program to print the value of gravity and the print the letter
used to represent the acceleration due to gravity.
 */


class Gravity{
	public static void main(String [] arga){
	
		float gravity=9.81f;
		char symbol='g';

		System.out.println("Gravity is "+gravity+"\nGravity Symbol "+symbol);
	}

}
