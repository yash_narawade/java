/*
8. Write a program to print the percentage and grade of a student.
*/


class PercentageAndGrade{
	public static void main(String [] args){
		
		float percentage =80.22f;
		char  grade = 'B';

		System.out.println("Percentage is "+percentage+"\nGrade "+grade);
	}
}
