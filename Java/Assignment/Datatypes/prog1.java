/*
1. A student is in class 11th and there are 12 divisions of the class which
data type is feasible to print the class and divisions. (Write a code to
print the class and division).
*/


class ClassAndDivision{
	public static void main( String [] args){
		
		byte studclass = 11;
		char division  = 'c';
		System.out.println("The student in class "+studclass +"th "+division);
	}
}
