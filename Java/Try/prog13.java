class Outer{
	int x=10;

	// Nested class 1
	class Inner1{
		
		 int x=20;

		Inner1(int x){
			System.out.println("Inner 1");
			System.out.println(Outer.this.x);
			System.out.println(this.x);
			System.out.println(x);
			Outer.this.fun();

		}
	
		// Nested class 1
		class Inner2{
		
			int x=40;
			Inner2(int x){
				
				System.out.println("Inner2 fun");
				System.out.println(Inner1.this.x);
				System.out.println(this.x);
				System.out.println(x);
				Inner1.this.fun();
			}
		}
		
		
		void fun(){
			System.out.println("Inner1 fun");
		}
		
	
	}

	void fun(){
	
		System.out.println("Outer fun");
	}


	public static void main(String [] args){
	
		
		Inner1.Inner2 obj =new Outer().new Inner1(30).new Inner2(50);
	}
}
