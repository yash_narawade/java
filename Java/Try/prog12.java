class prog12{

	public static void main(String [] args){
	
		int a=5;      //0101
		int b=10;	  //1010

		System.out.println(a);
		System.out.println(b);
		
		a=a^b; // 1111
		b=a^b; //0101  //b=5
		a=a^b; //1010  //a=10

		System.out.println(a);
		System.out.println(b);

	}
}
