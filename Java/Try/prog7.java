
// Hierarchical Inheritance


class Iphone{

	String Display="XDR";
	String Software="IOS17";

	Iphone(){
	
		System.out.println("Existing Feature");
		System.out.println("Display : "+Display);
		System.out.println("Software : "+Software);
	}
		void call(){
	
			System.out.println("Making Phone call");
		}


		void Connectivity() {
	
			System.out.println("Supports USB-C");
			System.out.println("Supports Faster Data Transfer");
			System.out.println("Face Recognination");
		}

		void colors(){
	
			System.out.println("Green , White , Black , Blue");
		}


	
}

class Iphone15 extends Iphone{

	float Display_size= 6.1f;
	String notch="Dynamic Island";
	String processor="BIONIC A16";
	Iphone15(){
	
		super.call();
		super.Connectivity();
		super.colors();
		
		System.out.println("\nNewly added Feature");
		System.out.println("Display Size  : "+Display_size);
		System.out.println("Notch : "+notch);
		System.out.println("Processor: "+processor);
	}
		void camera(){
		
			System.out.println("Upgrade camera form 40 to 48mp and automatic Portrate");
		}
	

}
class Iphone15_ProMax extends Iphone{
	String processor1= "BIONIC A17";
	String wifi="Wifi 6E";
	
	Iphone15_ProMax(){
	
		super.call();
		super.Connectivity();
		super.colors();
		
		System.out.println("\nNewly added Feature");
		System.out.println("processor : "+processor1);
		System.out.println("Wifi : "+wifi);
	}
	
		void Action(){
		
			System.out.println("Custmisable Action Button");
		}
	

	public static void main(String [] args){
	
		System.out.println("Iphone 15 details ");
		Iphone15 obj1 = new Iphone15();

		
		System.out.println("\nIphone 15 details ");
		Iphone15_ProMax obj2 = new Iphone15_ProMax();
	}

}
