class Outer{

	int x=10;

	class Inner{
		int x=40;	
		int y=20;
		int z=30;
		Inner(){
			
			System.out.println(Outer.this.x);
			System.out.println(y);
			System.out.println(z);
			System.out.println(x);
			Outer.this.fun();
			fun();
		}

		void fun(){
		
			System.out.println("In fun - Inner");
		}
	}

	void fun(){
	
		System.out.println("In fun - outer");
	}

	public static void main(String [] args){
	
	
		Outer obj1 = new Outer();
		Inner obj2 =obj1.new Inner();
	}
}
