class Demo{

	Demo(){
		this(10);
		System.out.println("Constructor with no parameter");
	}
	Demo(int x){
		this(10,20);	
		System.out.println("Constructor with 1-parameter");
	}
	Demo(int x,int y){
	
		System.out.println("Constructor with 2-parameter");
	}
}
class Child extends Demo{

	Child(){
		super();	
		System.out.println("In child constructor");
	}	
	public static void main(String [] args){
	
		Child obj = new Child();
	}


}
